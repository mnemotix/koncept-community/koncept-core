/**
 *
 *
 * Shortcut file, in order to be able to write in client code :
 *
 *    import [...] from '@mnemotix/koncept-core/server'
 *
 */

module.exports = require('./lib/server');
