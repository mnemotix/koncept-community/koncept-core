export const ROUTES = {
  INDEX: "/",
  SIGN_IN: "/",
  SIGN_UP: "/signup",
  PROFIL_PASSWORD_FORGOTTEN: "/resetpassword",
  VOCABULARY: "/voc/:id",
  VOCABULARY_RELATIONS: "/voc/:id/relations"
};
