/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createTheme} from '@mui/material/styles';
import {grey, blueGrey} from '@mui/material/colors';

const linkColor = '#b2adb5';

export function generateTheme({extraTheme} = {}) {
  return createTheme(
    {
      palette: {
        type: "light",
        primary: blueGrey,
        secondary: blueGrey,
        text: {
          emptyHint: grey["50"]
        }
      },
      typography: {
        fontSize: 12,
        h1: {
          fontSize: "3rem"
        },
        h2: {
          fontSize: "2.5rem"
        },
        h3: {
          fontSize: "1.75rem"
        },
        h4: {
          fontSize: "1.5rem"
        },
        h5: {
          fontSize: "1.4rem"
        },
        h6: {
          fontSize: "1.3rem"
        }
      },
      components: {
        MuiCssBaseline: {
          styleOverrides: {
            a: {
              color: linkColor,
              textDecoration: "none"
            },
          }
        },
        MuiLink: {
          styleOverrides: {
            root: {
              color: linkColor,
              textDecoration: "none"
            }
          }
        },
      }
    },
    extraTheme
  );
}
