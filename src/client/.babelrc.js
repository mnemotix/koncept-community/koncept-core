module.exports = function (api) {
  const presets = [
    [
      "@babel/preset-env",
      {
        modules: api.env("modern") ? false : "commonjs",
        browserslistEnv: process.env.NODE_ENV,
        shippedProposals: api.env("modern"),
        bugfixes: true
      }
    ],
    [
      "@babel/preset-react",
      {
        runtime: "automatic"
      }
    ]
  ];

  const plugins = [
    ["@babel/plugin-transform-runtime"],
    ["jsx-control-statements"],
    [
      "babel-plugin-import",
      {
        "libraryName": "@mui/material",
        "libraryDirectory": "",
        "camel2DashComponentName": false
      },
      "core"
    ],
    [
      "babel-plugin-import",
      {
        "libraryName": "@mui/icons-material",
        "libraryDirectory": "",
        "camel2DashComponentName": false
      },
      "icons"
    ],
    [
    "babel-plugin-import",
    {
      libraryName: "lodash",
      libraryDirectory: ".",
      camel2DashComponentName: false
    },
    "lodash"
    ]
  ];

  return {
    presets,
    plugins,
    sourceMaps: "inline"
  };
};
