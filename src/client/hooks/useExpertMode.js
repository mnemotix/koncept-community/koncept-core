/*
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState, useEffect} from "react";
import {createContainer} from "react-tracked";
import {useLocalStorage} from "@synaptix/ui";

/**
 * react-tracked library is used to store global state.
 * @see https://react-tracked.js.org/docs/tutorial-01
 */
const {Provider, useTrackedState, useUpdate} = createContainer(({ } = {}) => {
  const [expertMode, setExpertMode] = useState(false);
  const {localStorage} = useLocalStorage();
  const [inited, setInited] = useState(false);

  useEffect(() => {
    (async () => {
      if (localStorage) {
        const expertMode = await localStorage.getItem("expertMode");
        setExpertMode(expertMode);
        setInited(true);
      }
    })();
  }, [localStorage]);

  useEffect(() => {
    if (localStorage && inited) {
      (async () => {
        await localStorage.setItem("expertMode", expertMode);
      })();
    }
  }, [expertMode, inited]);

  return [expertMode, setExpertMode];
});

export const ExpertModeProvider = Provider;
export const useExpertMode = () => [useTrackedState(), useUpdate()];
