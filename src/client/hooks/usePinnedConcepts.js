/*
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from "react";
import {useLocalStorage} from "@synaptix/ui";
import {createContainer} from "react-tracked";

/**
 * react-tracked library is used to store global state.
 * @see https://react-tracked.js.org/docs/tutorial-01
 */
const {Provider, useTrackedState, useUpdate} = createContainer(({ } = {}) => {
  const {localStorage} = useLocalStorage();
  const [pinnedConcepts, setPinnedConcepts] = useState([]);
  const [inited, setInited] = useState(false);

  useEffect(() => {
    (async () => {
      if (localStorage) {
        const savedPinnedConcepts = await localStorage.getItem("pinnedConcepts");
        if (savedPinnedConcepts) {
          setPinnedConcepts(savedPinnedConcepts || []);
        }
        setInited(true);
      }
    })();
  }, [localStorage]);

  useEffect(() => {
    if (localStorage && inited) {
      (async () => {
        await localStorage.setItem(
          "pinnedConcepts",
          pinnedConcepts.map((pinnedConcept) => {
            return JSON.parse(JSON.stringify(pinnedConcept));
          }) // Tracked objects are javascript Proxies, destructure them to store them in local forage.
        );
      })();
    }
  }, [pinnedConcepts, inited]);

  return [pinnedConcepts || [], setPinnedConcepts];
});

export const PinnedConceptsProvider = Provider;
export const usePinnedConcepts = () => [useTrackedState(), useUpdate()];
