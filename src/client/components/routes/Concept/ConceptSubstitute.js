import {useState, useEffect} from "react";
import { DynamicForm } from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useApolloClient, useLazyQuery, useMutation, useQuery} from "@apollo/client";
import {useSnackbar} from "notistack";
import {Typography, Skeleton, Paper, Box, Chip, FormLabel} from '@mui/material';


import {gqlUpdateConcept} from "./gql/UpdateConcept.gql";
import {ConceptAutocomplete} from "./ConceptAutocomplete";
import {useTrackedAction} from "../../../hooks/useTrackedAction";
import {useTrackedObject} from "../../../hooks/useTrackedObject";
import {gqlRemoveObject} from "../../widgets/ObjectRemove/gql/ObjectRemove.gql";
import {gqlConceptProperties} from "./gql/ConceptProperties.gql";
import {conceptFormDefinition} from "./form/Concept.form";
import {ConceptForm} from "./ConceptForm";

import merge from "deepmerge";
import omit from "lodash/omit"
const classes = {
  message: {
    my: 3,
    p: 1
  },
  helpMessage: {
    mt: 1,
  },
  taggings: {
    mt: 2
  }
};

/**
 *
 */

export const ConceptSubstitute = ({id, vocabulary} = {}) => {

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const [targetConceptId, setTargetConceptId] = useState();
  const apolloClient = useApolloClient();
  const [, setActionType] = useTrackedAction();
  const [, setTrackedObject] =  useTrackedObject(null);

  const {data: {concept} = {}} = useQuery(gqlConceptProperties, {
    variables: {id},
    fetchPolicy: "no-cache"
  });

  const [loadTargetConcept, {data: {concept: targetConcept} = {}, loading: loadingTargetConcept} = {}] = useLazyQuery(gqlConceptProperties, {
    fetchPolicy: "no-cache"
  })

  const [mutateConcept, {loading: subtituting}] = useMutation(gqlUpdateConcept);
  const [removeObject, {loading: removing}] = useMutation(gqlRemoveObject);

  useEffect(() => {
    if(targetConceptId){
      loadTargetConcept({ variables: {id: targetConceptId}})
    }
  }, [targetConceptId]);

  return (
    <>
      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.ACTIONS.SUBSTITUTE")}
      </Typography>

      <ConceptAutocomplete
        gqlVariables={vocabulary ? {filters: [`hasVocabulary:${vocabulary.id}`]} : null}
        creationEnabled={false}
        onSelect={handleSelectTarget}
      />

      <If condition={targetConcept}>
        <Paper variant={"outlined"} sx={classes.message}>
          <Typography gutterBottom variant="body1" sx={classes.resumeMessage}>
            {t("CONCEPT.SUBSTITUTE.RESUME_MESSAGE", {sourceConcept: concept?.prefLabel, targetConcept: targetConcept?.prefLabel})}
          </Typography>
          <Typography gutterBottom variant="body2" sx={classes.helpMessage}>
            {t("CONCEPT.SUBSTITUTE.HELP_MESSAGE", {sourceConcept: concept?.prefLabel, targetConcept: targetConcept?.prefLabel})}
          </Typography>
        </Paper>
        <DynamicForm
          singleColumnLayout
          object={targetConcept}
          initialAlterObject={merge.all([{}, targetConcept, omit(concept, ["id", "prefLabel"]) ])}
          formDefinition={conceptFormDefinition}
          mutateFunction={mutateConcept}
          formatMutationInput={ ({object, objectInput}) => {
            if(concept?.taggings?.totalCount > 0){
              objectInput.taggingInputs = concept?.taggings.edges.map(({node}) => ({id: node.id}));
            }
            return {
              objectId: object.id,
              objectInput
            }
          }}
          onSubmitSuccess={handleSubstitute}
          saving={subtituting || removing}
          submitActionLabel={t("CONCEPT.SUBSTITUTE.SUBMIT")}
        >
          <Choose>
            <When condition={loadingTargetConcept}>
              <Skeleton variant="text" />
            </When>
            <Otherwise>
              <ConceptForm concept={concept} />

              <If condition={concept?.taggings?.totalCount > 0}>
                <Box sx={classes.taggings}>
                  <FormLabel >
                    {t("CONCEPT.PROPERTIES.TAGGINGS")}
                  </FormLabel>
                  <Box sx={classes.taggings}>
                    <Chip label={t("CONCEPT.PROPERTIES.TAGGINGS_ADD", {count: concept.taggings.totalCount})} variant={"outlined"}/>
                  </Box>
                </Box>
              </If>
            </Otherwise>
          </Choose>
        </DynamicForm>
      </If>
    </>
  );

  async function handleSelectTarget({id}){
    setTargetConceptId(id);
  }

  async function handleSubstitute(mutationResult) {
    await removeObject({
      variables: {
        input: {
          objectId: id
        }
      }
    });

    enqueueSnackbar(t("CONCEPT.SUBSTITUTE.SUCCESS_MESSAGE"), {variant: "success"});

    setTimeout(async () => {
      await apolloClient.refetchQueries({
        updateCache(cache) {
          cache.evict({ id });
          cache.evict({id: targetConcept.id});
        },
      });
      setActionType(null);
      setTrackedObject(targetConcept);
    }, 2000);
  }
};
