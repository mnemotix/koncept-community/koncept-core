import {Button, LinkEditDialog} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useApolloClient, useMutation, useQuery} from "@apollo/client";
import {useSnackbar} from "notistack";
import {Typography, ToggleButtonGroup, ToggleButton} from '@mui/material';

import {conceptRelationships, conceptSchemeLinkInputDefinition} from "./form/Concept.form";
import {useState} from "react";
import {gqlUpdateConcept} from "./gql/UpdateConcept.gql";
import {SchemeAutocomplete} from "../Scheme/SchemeAutocomplete";
import {gqlConceptMove} from "./gql/ConceptMove.gql";
import {ConceptAutocomplete} from "./ConceptAutocomplete";
import {useTrackedAction} from "../../../hooks/useTrackedAction";
import {useTrackedObject} from "../../../hooks/useTrackedObject";
import {gqlConcepts} from "./gql/Concepts.gql";
import {getConceptsColumnGqlVariables} from "../Vocabulary/TreeView/ConceptsColumn";


const classes = {
  confirmMessage: (theme) => ({
    margin: theme.spacing(2, 0)
  })
};

/**
 *
 */

export const ConceptMove = ({id, vocabulary} = {}) => {

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const [targetedObject, setTargetedObject] = useState();
  const [destination, setDestination] = useState();
  const [editDialogOpen, setEditDialogOpen] = useState(false);
  const apolloClient = useApolloClient();
  const [, setActionType] = useTrackedAction();
  const [, setTrackedObject] =  useTrackedObject(null);

  const {data: {concept} = {}} = useQuery(gqlConceptMove, {
    variables: {id},
    fetchPolicy: "no-cache"
  });

  const [mutateConcept, {loading: saving}] = useMutation(gqlUpdateConcept);

  return (
    <>
      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.ACTIONS.MOVE")}
      </Typography>

      <ToggleButtonGroup
        size="small"
        value={destination}
        exclusive
        onChange={(_, destination) => {
          setTargetedObject(null);
          setDestination(destination);
          setEditDialogOpen(true);
        }}>
        <ToggleButton value="scheme">{t("CONCEPT.ACTIONS.CHANGE_SCHEME")}</ToggleButton>
        <ToggleButton value="broader">{t("CONCEPT.ACTIONS.CHANGE_BROADER")}</ToggleButton>
      </ToggleButtonGroup>

      <If condition={targetedObject}>
        <Typography gutterBottom variant="body1" sx={classes.confirmMessage}>
          {destination === "broader"
            ? t("CONCEPT.ACTIONS.MOVE_TO_BROADER", {concept: targetedObject?.prefLabel})
            : t("CONCEPT.ACTIONS.MOVE_TO_SCHEME", {scheme: targetedObject?.prefLabel})}
        </Typography>
        <Button variant="contained" color="secondary" loading={saving} onClick={handleMove}>
          {t("CONCEPT.ACTIONS.MOVE")}
        </Button>
      </If>

      <LinkEditDialog
        object={concept}
        linkInputDefinition={
          destination === "broader"
            ? conceptRelationships.find(({key}) => key === "broaderConcept").linkInputDefinition
            : conceptSchemeLinkInputDefinition
        }
        renderObjectAutocomplete={({...props}) =>
          destination === "broader" ? (
            <ConceptAutocomplete
              {...props}
              gqlVariables={vocabulary ? {filters: [`hasVocabulary:${vocabulary.id}`]} : null}
              creationEnabled={false}
            />
          ) : (
            <SchemeAutocomplete
              {...props}
              gqlVariables={{filters: [`hasVocabulary:${vocabulary.id}`]}}
              creationEnabled={false}
            />
          )
        }
        open={editDialogOpen}
        titleLabel={
          destination === "broader" ? t("CONCEPT.ACTIONS.CHOOSE_BROADER") : t("CONCEPT.ACTIONS.CHOOSE_SCHEME")
        }
        onSave={handleChooseDestinationObject}
        onCancel={() => setEditDialogOpen(false)}
      />
    </>
  );

  function handleChooseDestinationObject(object) {
    setTargetedObject(object);
    setEditDialogOpen(false);
  }

  async function handleMove() {
    let topInSchemeInputs = [];
    let broaderConceptInput = null;
    const topInSchemeInputsToDelete = concept.topInSchemes.edges.map(({node}) => node.id);

    if (destination === "broader") {
      broaderConceptInput = {id: targetedObject.id};
    } else {
      topInSchemeInputs = [{id: targetedObject.id}];
    }

    await mutateConcept({
      variables: {
        input: {
          objectId: id,
          objectInput: {
            topInSchemeInputs,
            broaderConceptInput,
            topInSchemeInputsToDelete
          }
        }
      }
    });

    enqueueSnackbar(t("CONCEPT.MOVE.SUCCESS_MESSAGE"), {variant: "success"});

    setTimeout(async () => {
      await apolloClient.refetchQueries({
        updateCache(cache) {
          cache.evict({ fieldName: destination === "broader" ? "concepts" : "schemes"});
          cache.evict({ id });
        },
      });
      setActionType(null);
      setTrackedObject(targetedObject);
    }, 2000);
  }
};
