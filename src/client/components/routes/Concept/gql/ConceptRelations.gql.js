/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {LinkInputDefinition} from "@synaptix/ui";
import {gql} from "@apollo/client";
import {gqlConceptFragment} from "./Concept.gql";

/**
 * @param {LinkInputDefinition} linkInputDefinition
 * @returns {*}
 */
export function generateGqlConceptRelation({linkInputDefinition}) {
  return gql`
    query ConceptRelations($id: ID!) {
      concept(id: $id) {
        ...ConceptFragment
        ${linkInputDefinition.name}{
          ${linkInputDefinition.isPlural ? `edges { node {...ConceptFragment} }` : `...ConceptFragment`}
        }
      }
    }

    ${gqlConceptFragment}
  `;
}

export const gqlConceptRelations = gql`
  query ConceptDetails($id: ID!) {
    concept(id: $id) {
      ...ConceptFragment

      broaderConcept {
        ...ConceptFragment
      }

      narrowerConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      narrowerTransitiveConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      broaderTransitiveConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      relatedConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      exactMatchConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      closeMatchConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      relatedMatchConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }

      narrowerMatchConcepts {
        totalCount
        edges {
          node {
            ...ConceptFragment
          }
        }
      }
    }
  }

  ${gqlConceptFragment}
`;
