/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {DynamicFormDefinition, MutationConfig, LinkInputDefinition, TranslationsField} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlConceptPropertiesFragment} from "../gql/ConceptProperties.gql";
import {schemeFormDefinition} from "../../Scheme/form/Scheme.form";

export const conceptProperties = [
  {
    key: "prefLabelTranslations",
    i18n: "PREF_LABELS",
    toolTip: "skos:prefLabel",
    FieldComponent: TranslationsField
  },
  {
    key: "altLabelTranslations",
    i18n: "ALT_LABELS",
    toolTip: "skos:altLabel",
    FieldComponent: TranslationsField
  },
  {
    key: "hiddenLabelTranslations",
    i18n: "HIDDEN_LABELS",
    toolTip: "skos:hiddenLabel",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "notationTranslations",
    i18n: "NOTATION",
    toolTip: "skos:notation",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "definitionTranslations",
    i18n: "DEFINITION",
    toolTip: "skos:definition",
    FieldComponent: TranslationsField
  },
  {
    key: "exampleTranslations",
    i18n: "EXAMPLE",
    toolTip: "skos:example",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "scopeNoteTranslations",
    i18n: "SCOPE_NOTE",
    toolTip: "skos:scopeNote",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "changeNoteTranslations",
    i18n: "CHANGE_NOTE",
    toolTip: "skos:changeNote",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "historyNoteTranslations",
    i18n: "HISTORY_NOTE",
    toolTip: "skos:historyNote",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  },
  {
    key: "editorialNoteTranslations",
    i18n: "EDITORIAL_NOTE",
    toolTip: "skos:editorialNote",
    expertModeOnly: true,
    FieldComponent: TranslationsField
  }
];

export function getConceptValidationSchema({t}) {
  return object().shape({
    prefLabel: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))
  });
}

export const conceptFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: [...conceptProperties.map(({key}) => key)],
    gqlFragment: gqlConceptPropertiesFragment,
    gqlFragmentName: "ConceptPropertiesFragment"
  }),
  validationSchema: getConceptValidationSchema
});

export const conceptRelationships = [
  {
    key: "broaderConcept",
    i18n: "BROADER",
    toolTip: "skos:broader",
    sameVocabulary: true,
    linkInputDefinition: new LinkInputDefinition({
      name: "broaderConcept",
      inputName: "broaderConceptInput",
      targetObjectFormDefinition: conceptFormDefinition
    })
  },
  {
    key: "narrowerConcepts",
    i18n: "NARROWER",
    toolTip: "skos:narrower",
    sameVocabulary: true,
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerConcepts",
      isPlural: true,
      inputName: "narrowerConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    })
  },
  {
    key: "narrowerTransitiveConcepts",
    i18n: "NARROWER_TRANSITIVE",
    toolTip: "skos:narrowerTransitive",
    sameVocabulary: true,
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerTransitiveConcepts",
      isPlural: true,
      inputName: "narrowerTransitiveConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    }),
    expertModeOnly: true
  },
  {
    key: "broaderTransitiveConcepts",
    i18n: "BROADER_TRANSITIVE",
    toolTip: "skos:broaderTransitive",
    sameVocabulary: true,
    linkInputDefinition: new LinkInputDefinition({
      name: "broaderTransitiveConcepts",
      isPlural: true,
      inputName: "broaderTransitiveConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    }),
    expertModeOnly: true
  },
  {
    key: "relatedConcepts",
    i18n: "RELATED",
    toolTip: "skos:related",
    sameVocabulary: true,
    linkInputDefinition: new LinkInputDefinition({
      name: "relatedConcepts",
      isPlural: true,
      inputName: "relatedConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    })
  },
  {
    key: "exactMatchConcepts",
    i18n: "EXACT_MATCH",
    toolTip: "skos:exactMatch",
    linkInputDefinition: new LinkInputDefinition({
      name: "exactMatchConcepts",
      isPlural: true,
      inputName: "exactMatchConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    })
  },
  {
    key: "closeMatchConcepts",
    i18n: "CLOSE_MATCH",
    toolTip: "skos:closeMatch",
    linkInputDefinition: new LinkInputDefinition({
      name: "closeMatchConcepts",
      isPlural: true,
      inputName: "closeMatchConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    })
  },
  {
    key: "relatedMatchConcepts",
    i18n: "RELATED_MATCH",
    toolTip: "skos:relatedMatch",
    linkInputDefinition: new LinkInputDefinition({
      name: "relatedMatchConcepts",
      isPlural: true,
      inputName: "relatedMatchConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    }),
    expertModeOnly: true
  },
  {
    key: "broaderMatchConcepts",
    i18n: "BROADER_MATCH",
    toolTip: "skos:broaderMatch",
    linkInputDefinition: new LinkInputDefinition({
      name: "broaderMatchConcepts",
      isPlural: true,
      inputName: "broaderMatchConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    }),
    expertModeOnly: true
  },
  {
    key: "narrowerMatchConcepts",
    i18n: "NARROWER_MATCH",
    toolTip: "skos:narrowerrMatch",
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerMatchConcepts",
      isPlural: true,
      inputName: "narrowerMatchConceptInputs",
      targetObjectFormDefinition: conceptFormDefinition
    }),
    expertModeOnly: true
  }
];

export const conceptSchemeLinkInputDefinition = new LinkInputDefinition({
  name: "topInSchemes",
  inputName: "topInSchemeInputs",
  isPlural: true,
  targetObjectFormDefinition: schemeFormDefinition
});
