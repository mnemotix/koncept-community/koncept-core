import {MenuList, MenuItem, ListItemIcon, Grid, Typography, Divider, Skeleton} from "@mui/material";

import {useTranslation} from "react-i18next";
import {
  ArrowLeft as UnfoldIcon,
  ArrowRight as FoldIcon,
  MenuOpen as MenuOpenIcon,
  Visibility as OpenIcon,
  Edit as EditIcon,
  Add as AddIcon,
  Delete as RemoveIcon,
  Redo as MoveIcon,
  JoinFull as SubstituteIcon
} from "@mui/icons-material";
import {PinIcon} from "../../widgets/Icon/PinIcon";
import {PinIconFilled} from "../../widgets/Icon/PinIconFilled";
import {memo, useEffect, useState} from "react";
import {ConceptEdit} from "./ConceptEdit";
import {usePinnedConcepts} from "../../../hooks/usePinnedConcepts";
import {useTrackedAction} from "../../../hooks/useTrackedAction";
import {pickedPropsAreEquals} from "../../../utilities/pickedPropsAreEquals";
import {ConceptCreate} from "./ConceptCreate";
import {ObjectRemovePanel} from "../../widgets/ObjectRemove/ObjectRemovePanel";
import {ConceptMove} from "./ConceptMove";
import {ConceptSubstitute} from "./ConceptSubstitute";
import {useExtensionSettings} from "@synaptix/ui";
import {useTrackedObject} from "../../../hooks/useTrackedObject";


export const ConceptActions = {
  OPEN_NARROWERS: "OPEN_NARROWERS",
  CLOSE_NARROWERS: "CLOSE_NARROWERS",
  VIEW_DETAILS: "VIEW_DETAILS",
  SHOW_CONTEXT_MENU: "SHOW_CONTEXT_MENU",
  PIN: "PIN",
  UNPIN: "UNPIN",
  MOVE: "MOVE",
  REMOVE: "REMOVE",
  EDIT: "EDIT",
  ADD_CHILDREN: "ADD_CHILDREN",
  SUBSTITUTE: "SUBSTITUTE"
};

export function generateConceptActions({t, pinned, expanded, concept} = {}) {
  const {extendConceptActions} = useExtensionSettings("ConceptActions");

  let actions = [
    {
      key: ConceptActions.OPEN_NARROWERS,
      label: t("CONCEPT.ACTIONS.OPEN_NARROWERS"),
      description: "",
      IconComponent: FoldIcon,
      hidden: expanded,
      hiddenInContextMenu: true
    },
    {
      key: ConceptActions.CLOSE_NARROWERS,
      label: t("CONCEPT.ACTIONS.CLOSE_NARROWERS"),
      description: "",
      IconComponent: UnfoldIcon,
      hidden: !expanded,
      hiddenInContextMenu: true
    },
    {
      key: ConceptActions.VIEW_DETAILS,
      label: t("CONCEPT.ACTIONS.VIEW_DETAILS"),
      description: "",
      IconComponent: OpenIcon,
      tracked: true,
      hiddenInContextMenu: true
    },
    {
      key: ConceptActions.SHOW_CONTEXT_MENU,
      label: t("CONCEPT.ACTIONS.SHOW_CONTEXT_MENU"),
      description: "",
      IconComponent: MenuOpenIcon,
      tracked: true,
      hiddenInContextMenu: true
    },
    {
      key: ConceptActions.PIN,
      label: t("CONCEPT.ACTIONS.PIN"),
      description: "",
      IconComponent: PinIcon,
      hidden: pinned
    },
    {
      key: ConceptActions.UNPIN,
      label: t("CONCEPT.ACTIONS.UNPIN"),
      description: "",
      IconComponent: PinIconFilled,
      hidden: !pinned
    },
    {
      key: ConceptActions.EDIT,
      label: t("CONCEPT.ACTIONS.EDIT"),
      description: "",
      IconComponent: EditIcon,
      tracked: true,
      renderActionView: ({concept, vocabulary}) => <ConceptEdit id={concept?.id} vocabulary={vocabulary} />
    },
    {
      key: ConceptActions.MOVE,
      label: t("CONCEPT.ACTIONS.MOVE"),
      description: "",
      IconComponent: MoveIcon,
      tracked: true,
      renderActionView: ({concept, vocabulary}) => <ConceptMove id={concept?.id} vocabulary={vocabulary} />
    },
    {
      key: ConceptActions.ADD_CHILDREN,
      label: t("CONCEPT.ACTIONS.ADD_CHILDREN"),
      description: "",
      IconComponent: AddIcon,
      tracked: true,
      renderActionView: ({vocabulary, concept}) => <ConceptCreate vocabulary={vocabulary} broaderConcept={concept} />
    },
    {
      key: ConceptActions.SUBSTITUTE,
      label: t("CONCEPT.ACTIONS.SUBSTITUTE"),
      description: "",
      IconComponent: SubstituteIcon,
      tracked: true,
      renderActionView: ({vocabulary, concept}) => <ConceptSubstitute id={concept.id} vocabulary={vocabulary}  />
    },
    {
      key: ConceptActions.REMOVE,
      label: t("CONCEPT.ACTIONS.REMOVE"),
      description: "",
      IconComponent: RemoveIcon,
      tracked: true,
      renderActionView: ({concept, onRemoveSuccess}) => (
        <ObjectRemovePanel
          warningMessage={t("CONCEPT.REMOVE.WARNING_MESSAGE")}
          successMessage={t("CONCEPT.REMOVE.SUCCESS_MESSAGE")}
          objectId={concept.id}
          refetchQueries={["Concepts"]}
          onSuccess={onRemoveSuccess}
        />
      )
    }
  ];

  if (extendConceptActions) {
    actions = extendConceptActions({actions, t, pinned, expanded, concept});
  }

  return actions.filter((action) => !(action.hidden || false));
}

/**
 *
 */
const ConceptSidePopupActions = memo(({concept, vocabulary} = {}) => {

  const {t} = useTranslation();
  const [pinnedConcepts, setPinnedConcepts] = usePinnedConcepts();
  const pinned = pinnedConcepts.some(({id}) => id === concept.id);

  const actions = generateConceptActions({t, pinned, concept}).filter(({hiddenInContextMenu}) => !hiddenInContextMenu);
  const [selectedAction, setSelectedAction] = useState();
  const [actionType, setActionType] = useTrackedAction();
  const [selectedObject, setSelectedObject] = useTrackedObject(null);

  useEffect(() => {
    setSelectedAction(actions.find(({key}) => key === actionType));
  }, [actionType]);

  return (
    <>
      <Grid item xs>
        <Typography variant={"h5"} gutterBottom>
          {concept?.prefLabel ? t("CONCEPT.ACTIONS.TITLE", {concept: concept?.prefLabel}) : <Skeleton variant="text" />}
        </Typography>

        <Divider />
        <MenuList>
          {actions.map((action) => {
            const {key, label, description, IconComponent} = action;

            return (
              <MenuItem
                key={key}
                onClick={() => handleClickAction({action})}
                selected={selectedAction?.key === key}>
                <ListItemIcon>
                  <IconComponent />
                </ListItemIcon>
                {label}
              </MenuItem>
            );
          })}
        </MenuList>
      </Grid>
      <If condition={selectedAction}>
        <Grid item xs={7}>
          <If condition={selectedAction}>{selectedAction.renderActionView?.({concept, vocabulary, onRemoveSuccess: handleRemoveSuccess})}</If>
        </Grid>
      </If>
    </>
  );

  function handleClickAction({action}) {
    setSelectedAction(action);

    if (action.tracked) {
      setActionType(action.key);
      setSelectedObject(concept)
    }

    if (action.key === ConceptActions.PIN) {
      setPinnedConcepts([...pinnedConcepts, concept]);
    } else if (action.key === ConceptActions.UNPIN) {
      let mutatedPinnedConcepts = [...pinnedConcepts];
      const indexOf = mutatedPinnedConcepts.findIndex(({id}) => id === concept.id);
      mutatedPinnedConcepts.splice(indexOf, 1);
      setPinnedConcepts([...mutatedPinnedConcepts]);
    }
  }

  function handleRemoveSuccess(){
  }

}, pickedPropsAreEquals(["concept.id"]));

export default ConceptSidePopupActions;
