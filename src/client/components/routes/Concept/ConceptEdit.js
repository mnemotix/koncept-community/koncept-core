
import {useExtensionFragment, DynamicForm} from "@synaptix/ui";

import {useTranslation} from "react-i18next";
import {useMutation, useQuery} from "@apollo/client";
import {useSnackbar} from "notistack";
import {Typography, Tooltip, Skeleton} from '@mui/material';

import {conceptFormDefinition, conceptRelationships} from "./form/Concept.form";
import {gqlUpdateConcept} from "./gql/UpdateConcept.gql";
import {gqlConceptProperties} from "./gql/ConceptProperties.gql";
import {ConceptRelationEdit} from "./ConceptRelationEdit";
import {ConceptForm} from "./ConceptForm";
import {useExpertMode} from "../../../hooks/useExpertMode";

export const ConceptEdit = ({id, vocabulary} = {}) => {

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const [expertMode] = useExpertMode();
  const {data: {concept} = {}, loading} = useQuery(gqlConceptProperties, {
    variables: {id}
  });

  const [mutateConcept, {loading: saving}] = useMutation(gqlUpdateConcept, {
    onCompleted: (data) => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });

  return (
    <DynamicForm
      singleColumnLayout
      object={concept}
      formDefinition={conceptFormDefinition}
      mutateFunction={mutateConcept}
      saving={saving}

    >
      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.PROPERTIES.TITLE")}
      </Typography>

      <Choose>
        <When condition={loading}>
          <Skeleton variant="text" />
        </When>
        <Otherwise>
          <ConceptForm concept={concept} />
        </Otherwise>
      </Choose>

      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.RELATIONSHIPS.TITLE")}
      </Typography>

      {useExtensionFragment("ConceptRelationsEditPrepend", {concept})}

      {conceptRelationships
        .filter(({expertModeOnly}) => !expertModeOnly || expertMode === true)
        .map(({key, i18n, linkInputDefinition, toolTip}) => (
          <div key={key}>
            <Tooltip title={toolTip} placement="left">
              <Typography variant={"h6"} gutterBottom>
                {t("CONCEPT.RELATIONSHIPS." + i18n)}
              </Typography>
            </Tooltip>

            <ConceptRelationEdit id={id} linkInputDefinition={linkInputDefinition} vocabulary={vocabulary} />
          </div>
        ))}

      {useExtensionFragment("ConceptRelationsEditAppend", {concept})}
    </DynamicForm>
  );
};
