import {memo} from "react";
import {useExtensionFragment} from "@synaptix/ui";
import {func, string} from "prop-types";
import {List, ListItemButton, ListItemText, Tooltip, Typography, Box} from "@mui/material";

import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {gqlConceptRelations} from "./gql/ConceptRelations.gql";
import {InfiniteScrollColumn} from "../../widgets/Column/InfiniteScrollColumn";
import {conceptRelationships} from "./form/Concept.form";
import {pickedPropsAreEquals} from "../../../utilities/pickedPropsAreEquals";
import {useExpertMode} from "../../../hooks/useExpertMode";

const classes = {
  empty: {
    color: "text.emptyHint"
  },
  propertyItem: {
    marginBottom: 3
  }
};

/**
 *
 */
const ConceptRelations = memo(({id, onClickAssociatedConcept} = {}) => {
  const {t} = useTranslation();
  const [expertMode] = useExpertMode();
  const {data: {concept} = {}, loading} = useQuery(gqlConceptRelations, {
    variables: {id}
  });

  return (
    <>
      {useExtensionFragment("ConceptRelationsPrepend", {concept, onClickAssociatedConcept})}

      {conceptRelationships
        .filter(({expertModeOnly}) => !expertModeOnly || expertMode === true)
        .map(({key, i18n, sameVocabulary, toolTip, linkInputDefinition}) => {
          const isPlural = linkInputDefinition.isPlural;
          const totalCount = concept?.[key]?.totalCount;

          return (
            <Box key={key} sx={classes.propertyItem}>
              <Tooltip title={toolTip} placement="left">
                <Typography variant={"h6"} gutterBottom>
                  {t("CONCEPT.RELATIONSHIPS." + i18n)}
                </Typography>
              </Tooltip>

              <Choose>
                <When condition={loading || (isPlural && totalCount > 0)}>
                  <InfiniteScrollColumn
                    dense={true}
                    hasNextPage={false}
                    loading={loading}
                    onLoadNextPage={() => { }}
                    totalCount={totalCount}>
                    {concept?.[key].edges
                      .filter(({node: concept}) => concept.id !== id)
                      .map(({node: concept}) => (
                        <ListItemButton key={concept.id} onClick={() => onClickAssociatedConcept({concept})}>
                          <ListItemText
                            primary={concept.prefLabel}
                            secondary={sameVocabulary ? concept.schemePrefLabel : concept.vocabularyPrefLabel}
                          />
                        </ListItemButton>
                      ))}
                  </InfiniteScrollColumn>
                </When>
                <When condition={!isPlural && concept?.[key]}>
                  <List dense>
                    <ListItemButton onClick={() => onClickAssociatedConcept({concept: concept?.[key]})}>
                      <ListItemText
                        primary={concept?.[key].prefLabel}
                        secondary={sameVocabulary ? concept?.[key].schemePrefLabel : concept?.[key].vocabularyPrefLabel}
                      />
                    </ListItemButton>
                  </List>
                </When>
                <Otherwise>
                  <Box component="span" sx={classes.empty}>{t("CONCEPT.EMPTY_RELATIONSHIP")}</Box>
                </Otherwise>
              </Choose>
            </Box>
          );
        })}

      {useExtensionFragment("ConceptRelationsAppend", {concept, onClickAssociatedConcept})}
    </>
  );
}, pickedPropsAreEquals(["id"]));

ConceptRelations.propTypes = {
  id: string.isRequired,
  onClickAssociatedConcept: func.isRequired
};

export default ConceptRelations;
