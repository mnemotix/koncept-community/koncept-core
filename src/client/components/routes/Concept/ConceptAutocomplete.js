import {Autocomplete} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {ListItemText} from '@mui/material';
import {gqlConcepts} from "./gql/Concepts.gql";


/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} gqlVariables
 * @param {object} selectedObject
 * @param {string} error
 * @param {boolean} creationEnabled
 * @param {function} onCreate
 */

export function ConceptAutocomplete({
  onSelect,
  placeholder,
  selectedObject,
  error,
  creationEnabled,
  onCreate,
  gqlVariables
} = {}) {
  const {t} = useTranslation();

  return (
    <Autocomplete
      placeholder={placeholder || t("CONCEPT.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlConcepts}
      gqlVariables={gqlVariables}
      gqlDataConnectionPath={"concepts"}
      optionLabelPath={"prefLabel"}
      onSelect={onSelect}
      selectedObject={selectedObject}
      creationEnabled={creationEnabled}
      onCreate={onCreate}
      AutocompleteProps={{
        noOptionsText: t("CONCEPT.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
      renderOption={(concept) => (
        <ListItemText primary={concept.prefLabel} secondary={concept.schemePrefLabel} />
      )}
    />
  );
}
