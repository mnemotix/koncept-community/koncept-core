
import {useExtensionFragment, DynamicForm} from "@synaptix/ui";
import {Typography} from '@mui/material';
import {useTranslation} from "react-i18next";
import {useMutation, useApolloClient} from "@apollo/client";
import {useSnackbar} from "notistack";

import {conceptFormDefinition as conceptFormDefinitionDefault} from "./form/Concept.form";
import {gqlCreateConcept as gqlCreateConceptDefault} from "./gql/CreateConcept.gql";

import {ConceptForm} from "./ConceptForm";
import {useTrackedAction} from "../../../hooks/useTrackedAction";
import {useTrackedObject} from "../../../hooks/useTrackedObject";


/**
 /**
 * @param {object} vocabulary
 * @param {object} [scheme]
 * @param {object} [broaderConcept]
 * @param {boolean} [topInScheme=false]
 * @param {object} [conceptFormDefinition]
 * @param {object} [defaultGqlCreateConceptMutation]
 * @param {object} [ExtraFormComponent]
 * @returns {JSX.Element}
 * @constructor
 */
export const ConceptCreate = ({
  vocabulary,
  scheme,
  broaderConcept,
  topInScheme = false,
  conceptFormDefinition = conceptFormDefinitionDefault,
  gqlCreateConcept = gqlCreateConceptDefault,
  children
} = {}) => {
  if (!vocabulary) {
    throw new Error("A vocabulary must be passed");
  }

  if (!scheme && !broaderConcept) {
    throw new Error("A scheme OR broader concept must be passed");
  }

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const apolloClient = useApolloClient();
  const [, setActionType] = useTrackedAction();
  const [, setTrackedObject] =  useTrackedObject(null);

  const [mutateConcept, {loading: saving}] = useMutation(gqlCreateConcept);

  return (
    <DynamicForm
      singleColumnLayout
      formDefinition={conceptFormDefinition}
      mutateFunction={mutateConcept}
      mutateFunctionExtraParams={{
        update: handleCreateSuccess
      }}
      saving={saving}
      formatMutationInput={({objectInput}) => {
        objectInput.vocabularyInput = {
          id: vocabulary.id
        };

        if (scheme) {
          objectInput[topInScheme ? "topInSchemeInputs" : "schemeInputs"] = [
            {
              id: scheme.id
            }
          ];
        }

        if (broaderConcept) {
          objectInput.broaderConceptInput = {
            id: broaderConcept.id
          };
        }

        return {
          objectInput
        };
      }}>
      <Typography variant={"h6"} gutterBottom>
        {t("CONCEPT.PROPERTIES.TITLE")}
      </Typography>

      <ConceptForm />

      {useExtensionFragment("ConceptCreateAppend")}

      {children}
    </DynamicForm>
  );

  function handleCreateSuccess(cache, {data: {createConcept: {createdObject}}}){
    enqueueSnackbar(t("CONCEPT.CREATE.SUCCESS_MESSAGE"), {variant: "success"});

    setTimeout(async () => {
      await apolloClient.refetchQueries({
        updateCache(cache) {
          cache.evict({ fieldName: broaderConcept ? "concepts" : "schemes"});
          cache.evict({ id: broaderConcept?.id || scheme?.id });
        },
      });
    }, 2000);
  }
};
