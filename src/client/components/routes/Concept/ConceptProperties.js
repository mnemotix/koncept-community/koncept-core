import {string} from "prop-types";
import {useExtensionFragment} from "@synaptix/ui";
import {Box, Divider, List, ListItem, ListItemText, Tooltip, Typography, Skeleton, Chip} from '@mui/material';

import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {gqlConceptProperties} from "./gql/ConceptProperties.gql";
import {conceptProperties} from "./form/Concept.form";
import {memo} from "react";
import {pickedPropsAreEquals} from "../../../utilities/pickedPropsAreEquals";
import {useExpertMode} from "../../../hooks/useExpertMode";


const classes = {
  empty: {
    color: "text.emptyHint"
  },
  propertyItem: {
    marginTop: 3
  },
  langChip: {
    width: ({spacing}) => spacing(5),
    textTransform: 'uppercase'
  }
};

/**
 *
 */
const ConceptProperties = memo(({id, prefLabel} = {}) => {

  const {t} = useTranslation();
  const [expertMode] = useExpertMode();

  const {data: {concept} = {}, loading} = useQuery(gqlConceptProperties, {
    variables: {id}
  });

  return (
    <>
      <Typography variant={"h5"} gutterBottom>
        {concept?.prefLabel || prefLabel || <Skeleton variant="text" />}
      </Typography>
      <Divider />

      {useExtensionFragment("ConceptPropertiesPrepend", {concept})}

      {conceptProperties
        .filter(({expertModeOnly}) => !expertModeOnly || expertMode === true)
        .map(({key, i18n, toolTip}) => (
          <Box key={key} sx={classes.propertyItem}>
            <Tooltip title={toolTip} placement="left">
              <Typography variant={"h6"} gutterBottom>
                {t("CONCEPT.PROPERTIES." + i18n)}
              </Typography>
            </Tooltip>

            <Choose>
              <When condition={loading}>
                <Skeleton variant="text" />
              </When>
              <When condition={Array.isArray(concept?.[key]) && concept?.[key].length > 0}>
                <List dense disablePadding>
                  {concept[key].map(({value, lang}) => {
                    return (
                      <ListItem key={value} disableGutters secondaryAction={<Chip sx={classes.langChip} label={lang}  size="small" />}>
                        <ListItemText primary={value} />
                      </ListItem>
                    );
                  })}
                </List>
              </When>
              <When condition={concept?.[key] && !Array.isArray(concept?.[key])}>{concept?.[key]}</When>
              <Otherwise>
                <Box component="span" sx={classes.empty}>{t("CONCEPT.EMPTY_PROPERTY")}</Box>
              </Otherwise>
            </Choose>
          </Box>
        ))}

      <If condition={concept?.taggings?.totalCount > 0}>
        <Box sx={classes.propertyItem}>
          <Typography variant={"h6"} gutterBottom>
            {t("CONCEPT.PROPERTIES.TAGGINGS")}
          </Typography>
          <Chip label={t("CONCEPT.PROPERTIES.TAGGINGS_COUNT", {count: concept?.taggings?.totalCount})} variant={"outlined"}/>
        </Box>
      </If>

      {useExtensionFragment("ConceptPropertiesAppend", {concept})}
    </>
  );
}, pickedPropsAreEquals(["id", "prefLabel"]));

ConceptProperties.propTypes = {
  id: string.isRequired,
  prefLabel: string
};

export default ConceptProperties;
