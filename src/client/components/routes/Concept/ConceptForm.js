
import {useExtensionFragment, TextField} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {Tooltip, Box} from "@mui/material";

import {useExpertMode} from "../../../hooks/useExpertMode";

import {conceptProperties} from "./form/Concept.form";

const classes = {
  field: {
    marginBottom: 3
  }
};

/**
 *
 */
export function ConceptForm({concept} = {}) {

  const [expertMode] = useExpertMode();
  const {t} = useTranslation();

  return (
    <>
      {useExtensionFragment("ConceptFormPrepend", {concept})}

      {conceptProperties
        .filter(({expertModeOnly}) => !expertModeOnly || expertMode === true)
        .map(({key, i18n, toolTip, FieldComponent = TextField}, index) => (
          <Box key={key} sx={classes.field}>
            <Tooltip title={toolTip} placement="left">
              <Box>
                <FieldComponent name={key} label={t("CONCEPT.PROPERTIES." + i18n)} autoFocus={index === 0} />
              </Box>
            </Tooltip>
          </Box>
        ))}

      {useExtensionFragment("ConceptFormAppend", {concept})}
    </>
  );
}
