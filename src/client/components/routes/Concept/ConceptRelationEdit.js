import {useEffect} from "react";
import {PluralLinkField, SingularLinkField} from "@synaptix/ui";

import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";

import {ListItemText} from '@mui/material';

import {Progress} from "../../widgets/Progress";
import {generateGqlConceptRelation} from "./gql/ConceptRelations.gql";
import {ConceptAutocomplete} from "./ConceptAutocomplete";



/**
 * @param {string} [id]
 * @param {LinkInputDefinition} linkInputDefinition
 * @param {string} [addButtonLabel]
 * @param {object} [vocabulary]
 */
export const ConceptRelationEdit = ({id, linkInputDefinition, addButtonLabel, vocabulary} = {}) => {

  const {t} = useTranslation();
  const [getConcept, {data: {concept} = {}, loading}] = useLazyQuery(generateGqlConceptRelation({linkInputDefinition}));

  useEffect(() => {
    if (id) {
      getConcept({variables: {id}});
    }
  }, [id]);

  const LinkField = linkInputDefinition.isPlural ? PluralLinkField : SingularLinkField;

  return loading ? (
    <Progress />
  ) : (
    <LinkField
      data={concept}
      linkInputDefinition={linkInputDefinition}
      renderObjectContent={(concept) => (
        <ListItemText primary={concept.prefLabel} secondary={concept.schemePrefLabel} />
      )}
      renderObjectAutocomplete={({...props}) => (
        <ConceptAutocomplete
          {...props}
          creationEnabled={false}
          gqlVariables={vocabulary ? {filters: [`hasVocabulary:${vocabulary.id}`]} : null}
        />
      )}
      addButtonLabel={
        addButtonLabel || linkInputDefinition.isPlural
          ? t("CONCEPT.ACTIONS.ADD_RELATION")
          : t("CONCEPT.ACTIONS.MODIFY_RELATION")
      }
      emptyRelationLabel={t("CONCEPT.RELATIONSHIPS.NONE")}
    />
  );
};
