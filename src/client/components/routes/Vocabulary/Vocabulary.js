import {useState, useEffect, useRef} from "react";
import {SearchBar, useLocalStorage} from "@synaptix/ui";
import isUndefined from "lodash/isUndefined";
import {Typography, Grid, Divider, Box, Button as MuiButton, IconButton, Popper, ClickAwayListener, Chip} from "@mui/material";
import {
  ArrowDropDown as ArrowDropDownIcon,
  KeyboardArrowDown as ChevronDown,
  Edit as EditIcon
} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {useQuery, useApolloClient} from "@apollo/client";
import {generatePath, Route, Switch, useHistory} from "react-router-dom";
import {useParams, matchPath, Link} from "react-router-dom";

import {Progress, ROUTES} from "../../../index";
import {gqlVocabulary} from "./gql/Vocabulary.gql";
import {PinnedConcepts} from "./TreeView/PinnedConcepts";
import {isSidePopupWide, SidePopup} from "./TreeView/SidePopup";
import {ConceptActions} from "../Concept/ConceptSidePopupActions";
import {SchemeActions} from "../Scheme/SchemeSidePopupActions";
import {usePinnedConcepts} from "../../../hooks/usePinnedConcepts";
import {VocabulariesList} from "./VocabulariesList";
import {VocabularyColumns} from "./TreeView/VocabularyColumns";
import {Relations} from "./Relations/Relations";
import {gqlTrackedConcept} from "../Concept/gql/Concept.gql";
import {VocabularyActions} from "./VocabularySidePopupActions";
import {useTrackedAction, TrackedActionProvider} from "../../../hooks/useTrackedAction";
import {useTrackedObject, TrackedObjectProvider} from "../../../hooks/useTrackedObject";
import {VIRTUAL_DRAFT_SCHEME_ID} from "./TreeView/SchemesColumn";

const actionsBarHeight = 6;
const topBarHeight = 6;
const availableWidth = window.innerWidth;
const columnWidth = Math.min(availableWidth / 5, 400);

const classes = {
  root: {
    height: "100%"
  },
  topBar: (theme) => ({
    flexBasis: theme.spacing(topBarHeight)
  }),
  titleContainer: (theme) => ({
    padding: theme.spacing(2, 2, 0, 2),
    borderBottom: `1px solid ${theme.palette.grey["300"]}`,
    [theme.breakpoints.down("lg")]: {
      padding: theme.spacing(2, 1, 0, 1)
    }
  }),
  title: (theme) => ({
    fontSize: "2rem",
    [theme.breakpoints.down("lg")]: {
      fontSize: "1.5rem"
    }
  }),
  vocEditButton: {
    marginLeft: 2,
    padding: 0,
    "& svg": {
      fontSize: "1em"
    }
  },
  bottomBar: {
    flexBasis: actionsBarHeight,
    width: "100%"
  },
  viewport: {
    position: "relative",
    overflow: "auto",
    flexGrow: 1
  },
  tabs: {
    width: "100%"
  },
  activeView: {
    color: "secondary.main"
  },
  splitViewContainer: {
    overflow: "hidden"
  },
  splitViewRow: {
    position: "relative",
    flexGrow: 1,
    overflow: "hidden"
  },
  closeParallelVocabularyButton: (theme) => ({
    position: "absolute",
    top: theme.spacing(1),
    right: theme.spacing(2)
  }),
  columns: {
    height: "100%",
    overflowX: "auto",
    overflowY: "hidden"
  },
  column: {
    minWidth: columnWidth,
    maxWidth: columnWidth, //TODO: Test it in 4K
    height: "100%",
    overflowY: "scroll",
    overflowX: "visible",
    borderLeft: "3px solid #bdbdbd",
    scrollbarWidth: 0,
    "&::-webkit-scrollbar": {
      display: "none"
    }
  },
  sidePopup: (theme) => ({
    boxShadow: theme.shadows[3],
    flexBasis: columnWidth
  }),
  sidePopupWide: (theme) => ({
    flexBasis: [2 * columnWidth, "!important"]
  }),
  actionButtonContainer: {
    position: "relative"
  },
  actionButton: (theme) => ({
    padding: 0,
    borderTop: "2px solid rgba(0, 0, 0, 0.23)",
    height: theme.spacing(actionsBarHeight),
    width: "100%",
    borderRadius: 0
  }),
  mainViewButton: {
    border: "2px solid rgba(0, 0, 0, 0.23)",
    borderTopWidth: 0,
    borderTop: "none",
    textAlign: "center"
  },
  actionButtonActive: {
    background: "white"
  },
  actionDialog: (theme) => ({
    width: "100%",
    height: "50vh",
    overflowY: "auto",
    boxShadow: theme.shadows[6],
    background: "white",
    zIndex: theme.zIndex.tooltip,
    textTransform: "initial",
    fontWeight: "initial"
  }),
  searchBar: (theme) => ({
    flexBasis: columnWidth,
    padding: theme.spacing(1),
    marginTop: 1,
    borderBottom: `1px solid ${theme.palette.grey["300"]}`
  }),
  searchBarActive: (theme) => ({
    background: "white",
    boxShadow: theme.shadows[6],
    borderBottomWidth: 0
  }),
  searchBarWide: {
    flexBasis: [2 * columnWidth, "!important"]
  },
  pinnedConceptsCountChip: {
    marginLeft: 1
  },
};


/**
 *
 */
export default function Vocabulary({
  id,
  statePersistenceDisabled,
  statePersistenceSalt,
  bottomBarHidden,
  searchDisabled,
  sidePopupHidden,
  onSelectObject = () => { },
  isolated = false
} = {}) {
  const {t} = useTranslation();
  const history = useHistory();
  const apolloClient = useApolloClient();
  const [selectedPath, setSelectedPath] = useState([]);
  const [pinnedConceptsDialogOpen, setPinnedConceptsDialogOpen] = useState(false);
  const [vocabulariesListDialogOpen, setVocabulariesListDialogOpen] = useState(false);
  const [pinnedConcepts] = usePinnedConcepts();
  const [parallelVocabularies, setParallelVocabularies] = useState([]);
  const [parallelSelectedObjects, setParallelSelectedObjects] = useState([]);
  const [searchQuery, setSearchQuery] = useState(null);
  const [actionType, setActionType] = useTrackedAction();
  const [trackedObject, setTrackedObject] =  useTrackedObject(null);
  const pinnedConceptsDialogButtonRef = useRef();
  const vocabulariesListDialogButtonRef = useRef();

  let vocSettings = {};

  if (!id) {
    id = decodeURIComponent(useParams().id);
  }

  const {data: {vocabulary} = {}, loading} = useQuery(gqlVocabulary, {
    variables: {
      id
    }
  });

  const {localStorage} = useLocalStorage();
  const persitenceId = `${statePersistenceSalt || ""}${id}`;

  if (!statePersistenceDisabled) {
    useEffect(() => {
      (async () => {
        if (!statePersistenceDisabled && localStorage) {
          vocSettings = await localStorage.getItem(persitenceId);

          if (vocSettings) {
            const {selectedPath, selectedObject, parallelVocabularies, parallelSelectedObjects} = vocSettings;

            setSelectedPath(selectedPath || []);
            setTrackedObject(selectedObject || vocabulary);
            setParallelVocabularies(parallelVocabularies || []);
            setParallelSelectedObjects(parallelSelectedObjects || []);
          }
        }
      })();
    }, [localStorage]);
  }

  useEffect(() => {
    if (vocabulary && !trackedObject) {
      setTrackedObject(vocabulary);
    }
  }, [vocabulary]);

  useEffect(() => {
    if (!trackedObject) {
      handleUnselectAll();
    } else {
      switch (trackedObject.__typename) {
        case "Concept":
          return handleSelectConcept({concept: trackedObject, actionType});
        case "Scheme":
          return handleSelectScheme({scheme: trackedObject, actionType});
      }
    }
  }, [trackedObject, actionType]);

  const relationsViewActive = matchPath(history.location.pathname, {
    path: ROUTES.VOCABULARY_RELATIONS,
    exact: true
  });

  const sidePopuWide = isSidePopupWide(actionType) || !!searchQuery;

  return loading ? (
    <Progress />
  ) : (
    <Grid container sx={classes.root} direction="column" wrap={"nowrap"}>
      <Divider />
      <Grid container item sx={classes.topBar} wrap={"nowrap"}>
        <Grid item xs sx={classes.titleContainer}>
          <Typography sx={classes.title} variant={"h2"} gutterBottom>
            <Choose>
              <When condition={relationsViewActive}>
                <Box component="span" sx={classes.title}>Relations</Box>
              </When>
              <Otherwise>
                <Box component="span" sx={classes.titleButton}>
                  <If condition={vocabulary.altLabel}>
                    {vocabulary.altLabel}
                    {` / `}
                  </If>
                  <Typography
                    sx={classes.title}
                    display={"inline"}
                    variant={vocabulary.altLabel ? "h6" : "h2"}
                    component={"span"}>
                    {vocabulary.prefLabel}
                  </Typography>
                  <IconButton onClick={handleEditVocabulary} sx={classes.vocEditButton} size="small">
                    <EditIcon />
                  </IconButton>
                </Box>
              </Otherwise>
            </Choose>
          </Typography>
        </Grid>

        <If condition={!searchDisabled}>
          <Grid
            item
            sx={[
              classes.searchBar,
              sidePopuWide && classes.searchBarWide,
              !!searchQuery && classes.searchBarActive
            ]}>
            <SearchBar onChange={handleSearchChange} variant="outlined"/>
          </Grid>
        </If>
      </Grid >
      <Grid item container sx={classes.viewport} wrap={"nowrap"}>
        <Switch>
          <Route
            path={ROUTES.VOCABULARY_RELATIONS}
            render={() => (
              <Grid xs item>
                <Relations
                  onClose={() => history.goBack()}
                  selectedConcept={trackedObject?.__typename === "Concept" ? trackedObject : null}
                  onSelectConcept={handleSelectConcept}
                />
              </Grid>
            )}
          />

          <Route
            render={() => (
              <Grid xs item container direction={"column"} wrap={"nowrap"} sx={classes.splitViewContainer}>
                <Grid item sx={classes.splitViewRow} xs>
                  <VocabularyColumns
                    vocabularyId={id}
                    selectedPath={selectedPath}
                    selectedObject={trackedObject}
                    onSelectScheme={handleSelectScheme}
                    onSelectConcept={handleSelectConcept}
                    sxClass={classes.columns}
                    columnSxClass={classes.column}
                  />
                </Grid>

                {parallelVocabularies.map((vocabulary) => (
                  <Grid key={vocabulary.id} item sx={classes.splitViewRow} xs>
                    <TrackedActionProvider>
                      <TrackedObjectProvider>
                        <Vocabulary
                          id={vocabulary.id}
                          statePersistenceDisabled={statePersistenceDisabled}
                          bottomBarHidden
                          searchDisabled
                          sidePopupHidden
                          onSelectObject={handleSelectParallelVocabularyObject}
                          statePersistenceSalt={`${id}_splitted_`}
                          isolated
                        />
                      </TrackedObjectProvider>
                    </TrackedActionProvider>

                    <IconButton
                      sx={classes.closeParallelVocabularyButton}
                      onClick={handleCloseParallelVocabulary}>
                      <ChevronDown />
                    </IconButton>
                  </Grid>
                ))}
              </Grid>
            )}
          />
        </Switch>

        <If condition={!sidePopupHidden}>
          <Grid item sx={[classes.sidePopup, sidePopuWide && classes.sidePopupWide]}>
            <SidePopup
              vocabulary={vocabulary}
              sidePreviewedObjects={relationsViewActive ? [] : parallelSelectedObjects}
              searchQuery={searchQuery}
              searchInVocabulary={relationsViewActive ? null : vocabulary}
              onClickAssociatedConcept={handleSelectConcept}
            />
          </Grid>
        </If>
      </Grid>

      <If condition={!bottomBarHidden}>
        <Grid container item sx={classes.bottomBar}>
          <ClickAwayListener onClickAway={() => setVocabulariesListDialogOpen(false)}>
            <Grid item xs={4} sx={classes.actionButtonContainer}>
              <MuiButton
                ref={vocabulariesListDialogButtonRef}
                sx={[classes.actionButton, vocabulariesListDialogOpen && classes.actionButtonActive]}
                onClick={() => setVocabulariesListDialogOpen(!vocabulariesListDialogOpen)}>
                {t("VOCABULARY.TABS.SPLITTED_VIEWS")}
                <ArrowDropDownIcon />
              </MuiButton>

              <Popper
                disablePortal
                open={vocabulariesListDialogOpen}
                anchorEl={vocabulariesListDialogButtonRef?.current}
                placement={"top-start"}
                sx={classes.actionDialog}
                style={{marginLeft: "-5px"} /* This margin corrects a Popper.js ununderstandable offset*/}>
                <VocabulariesList onSelectVocabulary={handleSelectParallelVocabulary} creationDisabled />
              </Popper>
            </Grid>
          </ClickAwayListener>
          <Grid item xs={4} sx={classes.actionButtonContainer}  >
            <Box sx={[classes.actionButton, classes.mainViewButton]} id="mainViewButton">
              <MuiButton
                component={Link}
                to={generatePath(ROUTES.VOCABULARY, {id})}
                color={
                  matchPath(history.location.pathname, {path: ROUTES.VOCABULARY, exact: true}) ? "secondary" : "inherit"
                }>
                {t("VOCABULARY.TABS.VIEW_TREE")}
              </MuiButton>
              {/*&nbsp;{"/"}&nbsp;*/}
              {/*<MuiButton>{t("VOCABULARY.TABS.VIEW_COLLECTIONS")}</MuiButton>*/}
              &nbsp;{"/"}&nbsp;
              <MuiButton
                onClick={() => setActionType(null)}
                component={Link}
                to={generatePath(ROUTES.VOCABULARY_RELATIONS, {id})}
                color={relationsViewActive ? "secondary" : "inherit"}>
                {t("VOCABULARY.TABS.RELATIONS")}
              </MuiButton>
            </Box>
          </Grid>
          <ClickAwayListener onClickAway={() => setPinnedConceptsDialogOpen(false)}>
            <Grid item xs={4} sx={classes.actionButtonContainer}>
              <MuiButton
                ref={pinnedConceptsDialogButtonRef}
                sx={[classes.actionButton, pinnedConceptsDialogOpen && classes.actionButtonActive]}
                onClick={() => setPinnedConceptsDialogOpen(!pinnedConceptsDialogOpen)}>
                {t("VOCABULARY.TABS.PINNED_CONCEPTS", {count: pinnedConcepts?.length || 0})}
                <If condition={pinnedConcepts?.length > 0}>
                  <Chip
                    color="secondary"
                    label={pinnedConcepts.length}
                    size="small"
                    sx={classes.pinnedConceptsCountChip}
                  />
                </If>
                <ArrowDropDownIcon />
              </MuiButton>

              <Popper
                disablePortal
                open={pinnedConceptsDialogOpen}
                anchorEl={pinnedConceptsDialogButtonRef?.current}
                placement={"top-start"}
                sx={classes.actionDialog}
                style={{marginRight: "-5px"} /* This margin corrects a Popper.js ununderstandable offset*/}>
                <PinnedConcepts pinned open={pinnedConceptsDialogOpen} onSelectConcept={handleSelectConcept} />
              </Popper>
            </Grid>
          </ClickAwayListener>
        </Grid>
      </If >
    </Grid >
  );

  async function handleEditVocabulary() {
    return mutateContext({
      selectedPath: [],
      selectedObject: vocabulary,
      sidePopupOpen: true,
      actionType: VocabularyActions.EDIT
    });
  }

  async function handleSelectScheme({scheme, actionType}) {
    let mutatedPath = selectedPath;
    let mutatedSidePopupOpen = false;

    switch (actionType) {
      case SchemeActions.CLOSE_NARROWERS:
        mutatedPath = [];
        break;
      case SchemeActions.SHOW_CONTEXT_MENU:
        mutatedSidePopupOpen = true;
        break;
      case SchemeActions.VIEW_DETAILS:
        mutatedSidePopupOpen = true;
        break;
      case SchemeActions.OPEN_NARROWERS:
      default:
        mutatedPath = [{...scheme}];
    }

    return mutateContext({
      selectedPath: mutatedPath,
      selectedObject: scheme,
      sidePopupOpen: mutatedSidePopupOpen,
      actionType
    });
  }

  /**
   * @param {object} concept
   * @param {string} actionType
   */
  async function handleSelectConcept({concept: {id}, actionType} = {}) {
    const {data: {concept} = {}} = await apolloClient.query({query: gqlTrackedConcept, variables: {id}});

    if(concept){
      const broaderConcepts = concept.broaderTransitiveConcepts.edges.map(({node}) => node);
      const scheme = concept.schemes.edges?.[0]?.node || {
        __typename: "Scheme",
        id: VIRTUAL_DRAFT_SCHEME_ID,
        vocabularyId: concept?.vocabulary?.id
      };

      let mutatedPath = [scheme, ...broaderConcepts, concept];
      let mutatedSidePopupOpen = false;
      let mutatedSelectedConcept = trackedObject;

      switch (actionType) {
        case ConceptActions.CLOSE_NARROWERS:
          mutatedPath = mutatedPath.slice(0, mutatedPath.length - 1);
          break;
        case ConceptActions.SHOW_CONTEXT_MENU:
          mutatedSidePopupOpen = true;
          break;
        case ConceptActions.VIEW_DETAILS:
          mutatedSidePopupOpen = true;
          mutatedSelectedConcept = concept;
          break;
        case ConceptActions.EDIT:
          mutatedSidePopupOpen = true;
          break;
      }

      return mutateContext({
        selectedPath: mutatedPath,
        selectedObject: concept,
        sidePopupOpen: mutatedSidePopupOpen,
        actionType
      });
    }
  }

  function handleSelectParallelVocabulary(vocabulary) {
    const parallelVocabularies = [vocabulary];
    setVocabulariesListDialogOpen(false);

    mutateContext({
      parallelVocabularies
    });
  }

  /**
   */
  function handleSelectParallelVocabularyObject({object, actionType}) {
    mutateContext({
      parallelSelectedObjects: [{...object}]
    });
  }

  function handleCloseParallelVocabulary() {
    mutateContext({
      parallelVocabularies: [],
      parallelSelectedObjects: []
    });
  }

  async function handleUnselectAll(){
    setActionType(null);

    return mutateContext({
      selectedPath: selectedPath.slice(0, selectedPath.length - 1),
      selectedObject: vocabulary,
      sidePopupOpen: false,
      actionType: null
    });
  }

  function handleSearchChange(searchQuery) {
    setSearchQuery(searchQuery !== "" ? searchQuery : null);
  }

  /**
   * @param {string[]} [mutatedSelectedPath]
   * @param {object} [selectedObject]
   * @param {boolean} [sidePopupOpen]
   * @param {object[]} [parallelVocabularies]
   * @param {object[]} [parallelSelectedObjects]
   * @param {string} [actionType]
   */
  async function mutateContext({
    selectedPath,
    selectedObject,
    sidePopupOpen,
    parallelVocabularies,
    parallelSelectedObjects,
    actionType
  }) {
    const persistedState = (await localStorage?.getItem(persitenceId)) || {};

    if (!isUndefined(selectedPath)) {
      setSelectedPath(selectedPath);
      persistedState.selectedPath = selectedPath;
    }

    if (!isUndefined(selectedObject)) {
      setTrackedObject(selectedObject);
      persistedState.selectedObject = {...selectedObject};
    }

    if (!isUndefined(parallelVocabularies)) {
      setParallelVocabularies(parallelVocabularies);
      persistedState.parallelVocabularies = parallelVocabularies;
    }

    if (!isUndefined(parallelSelectedObjects)) {
      setParallelSelectedObjects(parallelSelectedObjects);
      persistedState.parallelSelectedObjects = parallelSelectedObjects;
    }

    if (!statePersistenceDisabled) {
      localStorage?.setItem(persitenceId, persistedState);
    }

    onSelectObject({object: selectedObject, actionType});
  }
}
