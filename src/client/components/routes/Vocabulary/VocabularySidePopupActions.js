
import {useTranslation} from "react-i18next";
import {Edit as EditIcon, Add as AddIcon, Delete as RemoveIcon} from "@mui/icons-material";
import {memo, useEffect, useState} from "react";
import {pickedPropsAreEquals} from "../../../utilities/pickedPropsAreEquals";
import {Grid, MenuList, MenuItem, ListItemIcon, Typography, Divider} from "@mui/material";
import {VocabularyEdit} from "./VocabularyEdit";
import {useTrackedAction} from "../../../hooks/useTrackedAction";
import {SchemeCreate} from "../Scheme/SchemeCreate";
import {useHistory} from "react-router-dom";
import {ObjectRemovePanel} from "../../widgets/ObjectRemove/ObjectRemovePanel";
import {ROUTES} from "../../../routes";
import {gqlVocabularies} from "./gql/Vocabularies.gql";

const classes = {
  column: {
    padding: 2
  }
};

export const VocabularyActions = {
  REMOVE: "REMOVE",
  ADD_SCHEME: "ADD_SCHEME",
  EDIT: "EDIT"
};

export function generateVocabularyActions({t, history} = {}) {
  return [
    {
      key: VocabularyActions.EDIT,
      label: t("VOCABULARY.ACTIONS.EDIT"),
      description: "",
      IconComponent: EditIcon,
      tracked: true,
      renderActionView: ({vocabulary}) => <VocabularyEdit id={vocabulary?.id} />
    },
    {
      key: VocabularyActions.ADD_SCHEME,
      label: t("VOCABULARY.ACTIONS.ADD_SCHEME"),
      description: "",
      IconComponent: AddIcon,
      tracked: true,
      renderActionView: ({vocabulary}) => <SchemeCreate vocabulary={vocabulary} />
    },
    {
      key: VocabularyActions.REMOVE,
      label: t("VOCABULARY.ACTIONS.REMOVE"),
      description: "",
      IconComponent: RemoveIcon,
      tracked: true,
      renderActionView: ({vocabulary}) => (
        <ObjectRemovePanel
          warningMessage={t("VOCABULARY.REMOVE.WARNING_MESSAGE")}
          successMessage={t("VOCABULARY.REMOVE.SUCCESS_MESSAGE")}
          objectId={vocabulary.id}
          onSuccess={() => history.push(ROUTES.INDEX)}
          refetchQueries={[{query: gqlVocabularies}]}
        />
      )
    }
  ].filter((action) => !(action.hidden || false));
}

/**
 *
 */
const VocabularySidePopupActions = memo(({vocabulary} = {}) => {

  const {t} = useTranslation();
  const history = useHistory();
  const actions = generateVocabularyActions({t, history});
  const [selectedAction, setSelectedAction] = useState();
  const [actionType, setActionType] = useTrackedAction();
  useEffect(() => {
    setSelectedAction(actions.find(({key}) => key === actionType));
  }, [actionType]);

  return (
    <>
      <Grid item xs sx={classes.column}>
        <Typography variant={"h5"} gutterBottom>
          {vocabulary?.prefLabel || <Skeleton variant="text" />}
        </Typography>

        <Divider />
        <MenuList>
          {actions.map((action) => {
            const {key, label, description, IconComponent} = action;

            return (
              <MenuItem
                key={key}
                onClick={() => handleClickAction({action})}
                selected={selectedAction?.key === key}>
                <ListItemIcon>
                  <IconComponent />
                </ListItemIcon>
                {label}
              </MenuItem>
            );
          })}
        </MenuList>
      </Grid>
      <If condition={selectedAction}>
        <Grid item xs={7} sx={classes.column}>
          <If condition={selectedAction}>{selectedAction.renderActionView?.({vocabulary})}</If>
        </Grid>
      </If>
    </>
  );

  function handleClickAction({action}) {
    setSelectedAction(action);
    setActionType(action.key);

    if (action.tracked) {
      setActionType(action.key);
    }
  }
}, pickedPropsAreEquals([""]));

export default VocabularySidePopupActions;
