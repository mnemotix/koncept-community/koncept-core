import {Grid} from "@mui/material";
import {SchemesColumn} from "./SchemesColumn";
import {ConceptsColumn} from "./ConceptsColumn";

/**
 *
 */
export function VocabularyColumns({
  vocabularyId,
  selectedObject,
  selectedPath,
  onSelectScheme,
  onSelectConcept,
  sxClass,
  columnSxClass
} = {}) {


  return (
    <Grid container item sx={sxClass} wrap={"nowrap"}>
      <SchemesColumn
        dense
        vocabularyId={vocabularyId}
        onSelectScheme={onSelectScheme}
        selectedScheme={selectedObject}
        expandedScheme={selectedPath[0]}
        sxClass={columnSxClass}
      />

      {selectedPath.map((object, index) => {
        const narrowersCount = object.__typename === "Scheme" ? object.topConceptsCount : object.narrowerConceptsCount;

        return (
          <ConceptsColumn
            key={object.id}
            dense
            broaderObject={object}
            sxClass={columnSxClass}
            onSelectConcept={({concept, actionType}) => onSelectConcept({concept, actionType, depth: index + 1})}
            expandedConcept={selectedPath[index + 1]}
            selectedConcept={selectedObject}
            estimatedConceptsCount={narrowersCount}
          />
        );
      })}
    </Grid>
  );
}
