import {useState, useEffect, useRef} from "react";
import {
  IconButton, ListItemButton, ListItemText, Divider, Box,
  SpeedDial, SpeedDialIcon, SpeedDialAction
} from "@mui/material";

import {Close as CloseIcon, MoreHoriz as MoreHorizIcon, ArrowRight as ExpandIcon} from "@mui/icons-material";

import {PinIcon} from "../../../widgets/Icon/PinIcon";
import {PinIconFilled} from "../../../widgets/Icon/PinIconFilled";

import {useTranslation} from "react-i18next";
import {useDoubleClick} from "../../../../hooks/useDoubleClick";
import {useTrackedAction} from "../../../../hooks/useTrackedAction";

const classes = {
  listItem: {
    paddingRight: 12,
    fontSize: "0.9rem"
  },
  expandButton: (theme) => ({
    position: "absolute",
    right: theme.spacing(0.5),
    left: "auto",
    top: theme.spacing(1),
    padding: theme.spacing(1.5, 1.2),
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.primary.main
    }
  }),
  pinButton: (theme) => ({
    position: "absolute",
    right: theme.spacing(5),
    left: "auto",
    top: theme.spacing(3),
    padding: theme.spacing(1.5, 1.2),
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.primary.main
    }
  }),
  subConceptsContainer: (theme) => ({
    padding: theme.spacing(1, 0),
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    height: theme.spacing(3)
  }),
  subConceptsSeparator: {
    flexGrow: 6
  },
  subConcepts: (theme) => ({
    flexGrow: 1,
    textAlign: "right",
    color: theme.palette.grey["400"]
  }),
  selectedMarker: (theme) => ({
    position: "absolute",
    right: 0,
    left: "auto",
    top: theme.spacing(2.1),
    borderTop: `solid 3px ${theme.palette.grey["400"]}`,
    width: theme.spacing(4)
  }),
  menu: (theme) =>  ({
    position: "absolute",
    right: theme.spacing(5),
    left: "auto",
    top: theme.spacing(0.2),
    '& .MuiSpeedDial-actions' : {
      background: "white",
      borderRadius: [theme.spacing(6), "!important"],
      marginRight: [theme.spacing(-5), "!important"],
      paddingRight: [theme.spacing(4), "!important"],
      boxShadow: theme.shadows[5],
      pointerEvents: "auto"
    },
    '& .MuiSpeedDial-actionsClosed' :  {
      background: "transparent",
      boxShadow: "none",
      pointerEvents: "none"
    }
  }),
  fabIcon: (theme) => ({
    margin: 0,
    boxShadow: "none",
    backgroundColor: "transparent",
    color: theme.palette.grey["400"],
    "&:hover": {
      backgroundColor: "transparent",
      color: theme.palette.grey["600"],
    },
    width: theme.spacing(4.5),
  }),
};

/**
 *
 */
export function ListItem({
  primaryText,
  subConceptsCount,
  pinnable = false,
  actions,
  onPin,
  onUnpin,
  onDoubleClick,
  onClick,
  onRightClick,
  onActionClick,
  expanded,
  selected,
  pinned,
  hideMenu,
  sxClass
} = {}) {
  const menuRef = useRef(null);
  const {t} = useTranslation();
  const [menuOpen, setMenuOpen] = useState(false);
  const hybridClick = useDoubleClick(handleDoubleClick, handleClick);
  useEffect(() => {
    const menu = menuRef?.current;

    if (!menu) {
      return;
    }

    menu.addEventListener("contextmenu", handleRightClick);

    return function cleanup() {
      menu.removeEventListener("contextmenu", handleRightClick);
    };
  }, []);


  return (
    <ListItemButton
      onClick={hybridClick}
      selected={selected}
      sx={[sxClass, classes.listItem]}
      ref={menuRef}>
      <ListItemText
        disableTypography
        primary={<Box sx={[classes.prefLabel, selected && classes.selected]}>{primaryText}</Box>}
        secondary={
          <Box sx={classes.subConceptsContainer}>
            <Box sx={classes.subConceptsSeparator}>
              <Divider />
            </Box>
            <If condition={subConceptsCount > 0}>
              <Box sx={classes.subConcepts}>{t("CONCEPT.NARROWERS_COUNT", {count: subConceptsCount})}</Box>
            </If>
          </Box>
        }
      />

      <If condition={!hideMenu}>
        <Choose>
          <When condition={expanded}>
            <Box sx={classes.selectedMarker} />
          </When>
          <When condition={subConceptsCount > 0}>
            <IconButton size={"small"} sx={classes.expandButton}>
              <ExpandIcon />
            </IconButton>
          </When>
        </Choose>

        <If condition={pinnable}>
          <IconButton
            sx={classes.pinButton}
            size="small"
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              pinned ? onUnpin() : onPin();
            }}>
            {pinned ? (
              <PinIconFilled fontSize="small" color={"secondary"} />
            ) : (
              <PinIcon fontSize="small" color={"disabled"} />
            )}
          </IconButton>
        </If>

        <Box sx={classes.actions}>
          <SpeedDial
            direction={"left"}
            icon={
              <SpeedDialIcon
                fontSize="small"
                openIcon={<CloseIcon fontSize="small" />}
                icon={<MoreHorizIcon fontSize="small" />}
              />
            }
            open={menuOpen}
            ariaLabel={""}
            onOpen={(event, reason) => {
              event.persist();
              event.preventDefault();
              event.stopPropagation();

              if (reason === "toggle") {
                setMenuOpen(true);
              }
            }}
            onClose={() => {
              setMenuOpen(false);
            }}
            FabProps={getFabProps()}
            sx={classes.menu}>
            {(actions || []).map(({IconComponent, label, key, tracked}) => renderAction({IconComponent, tooltip: label, key, tracked}))}
          </SpeedDial>
        </Box>
      </If>
    </ListItemButton>
  );

  function renderAction({IconComponent, onClick, tooltip, key, tracked}) {
    return (
      <SpeedDialAction
        key={key}
        icon={<IconComponent fontSize={"small"} />}
        tooltipTitle={tooltip}
        tooltipPlacement={"top"}
        onClick={(e) => {
          e.persist();
          e.preventDefault();
          e.stopPropagation();

          onActionClick(key, e);
        }}
        FabProps={getFabProps()}
      />
    );
  }

  function getFabProps() {
    return {
      size: "small",
      sx: classes.fabIcon
    };
  }

  function handleClick(e) {
    onClick(e);
  }

  function handleDoubleClick(e) {
    onDoubleClick(e);
  }

  function handleRightClick(e) {
    e.preventDefault();
    onRightClick(e);
  }
}
