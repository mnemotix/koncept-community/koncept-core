import {memo} from "react";
import {useTranslation} from "react-i18next";
import {ListItem} from "./ListItem";
import {SchemeActions, generateSchemeActions} from "../../Scheme/SchemeSidePopupActions";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";
import {useTrackedObject} from "../../../../hooks/useTrackedObject";
import {useTrackedAction} from "../../../../hooks/useTrackedAction";


/**
 *
 */
export const SchemeListItem = memo(({scheme, expanded, selected, pinned, hideMenu} = {}) => {
  const {t} = useTranslation();
  const [, setSelectedObject] = useTrackedObject(null);
  const [, setActionType] = useTrackedAction();

  return (
    <ListItem
      primaryText={scheme?.prefLabel}
      subConceptsCount={scheme?.topConceptsCount}
      expanded={expanded}
      selected={selected}
      hideMenu={hideMenu}
      actions={getActions()}
      onDoubleClick={handleDoubleClick}
      onClick={handleClick}
      onRightClick={handleRightClick}
      onActionClick={handleAction}
    />
  );

  function getActions() {
    return generateSchemeActions({t, pinned, expanded}).filter(
      ({hiddenInFabMenu}) => !hiddenInFabMenu
    );
  }
  function handleClick(e) {
    handleAction(SchemeActions.OPEN_NARROWERS, e);
  }

  function handleDoubleClick(e) {
    handleAction(SchemeActions.VIEW_DETAILS, e);
  }

  function handleRightClick(e) {
    handleAction(SchemeActions.SHOW_CONTEXT_MENU, e);
  }

  function handleAction(actionType, e) {
    setSelectedObject(scheme);
    setActionType(actionType);
  }
}, pickedPropsAreEquals(["expanded", "selected", "pinned", "scheme.id", "scheme.prefLabel"]));
