import {memo} from "react";
import {bool, object, func, string} from "prop-types";
import {useExtensionFragment} from "@synaptix/ui";

import {useTranslation} from "react-i18next";
import {ListItem} from "./ListItem";
import {ConceptActions, generateConceptActions} from "../../Concept/ConceptSidePopupActions";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";
import {usePinnedConcepts} from "../../../../hooks/usePinnedConcepts";
import {useTrackedObject} from "../../../../hooks/useTrackedObject";
import {useTrackedAction} from "../../../../hooks/useTrackedAction";



/**
 * @param {object} concept
 * @param {boolean} [expanded]
 * @param {boolean} [selected]
 * @param {boolean} [hideMenu]
 * @param {boolean} [sxClass]
 * @returns {JSX.Element}
 * @constructor
 */
export const ConceptListItem = memo(({concept, expanded, selected, hideMenu, sxClass} = {}) => {
  const {t} = useTranslation();
  const [pinnedConcepts, setPinnedConcepts] = usePinnedConcepts();
  const pinned = pinnedConcepts.some(({id}) => id === concept.id);
  const [, setTrackedObject] = useTrackedObject(null);
  const [, setActionType] = useTrackedAction();

  return (
    <ListItem
      sx={sxClass}
      primaryText={
        <>
          {useExtensionFragment("ConceptListItemBadge", {concept})} {concept?.prefLabel}
        </>
      }
      subConceptsCount={concept?.narrowerConceptsCount}
      expanded={expanded}
      selected={selected}
      hideMenu={hideMenu}
      pinned={pinned}
      pinnable
      actions={getActions()}
      onDoubleClick={handleAction.bind(this, ConceptActions.VIEW_DETAILS)}
      onClick={handleAction.bind(this, ConceptActions.OPEN_NARROWERS)}
      onRightClick={handleAction.bind(this, ConceptActions.SHOW_CONTEXT_MENU)}
      onPin={handleAction.bind(this, ConceptActions.PIN)}
      onUnpin={handleAction.bind(this, ConceptActions.UNPIN)}
      onActionClick={handleAction}
    />
  );

  function getActions() {
    return generateConceptActions({t, pinned, expanded}).filter(({hiddenInFabMenu}) => !hiddenInFabMenu);
  }

  function handleAction(actionType, e) {
    if (actionType === ConceptActions.PIN) {
      setPinnedConcepts([...pinnedConcepts, concept]);
    } else if (actionType === ConceptActions.UNPIN) {
      let mutatedPinnedConcepts = [...pinnedConcepts];
      const indexOf = mutatedPinnedConcepts.findIndex(({id}) => id === concept.id);
      mutatedPinnedConcepts.splice(indexOf, 1);
      setPinnedConcepts([...mutatedPinnedConcepts]);
    } else {
      setTrackedObject(concept);
      setActionType(actionType);
    }
  }
}, pickedPropsAreEquals(["expanded", "selected", "pinned", "concept.prefLabel", "concept.narrowerConceptsCount"]));

ConceptListItem.propTypes = {
  concept: object.isRequired,
  expanded: bool,
  selected: bool,
  pinned: bool,
  hideMenu: bool,
  className: string
};
