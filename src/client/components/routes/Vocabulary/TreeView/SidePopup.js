import {memo} from "react";
import {Grid, Divider, IconButton, Tabs, Tab, Typography} from "@mui/material";

import loadable from "@loadable/component";
import {
  ChevronLeft as OpenIcon,
  ChevronRight as CloseIcon,
  Info as InfoIcon,
  MenuOpen as MenuOpenIcon,
  Search as SearchIcon
} from "@mui/icons-material";

import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";
import VocabularySidePopupActions, {VocabularyActions} from "../VocabularySidePopupActions";
import {useTrackedAction} from "../../../../hooks/useTrackedAction";
import {useTrackedObject} from "../../../../hooks/useTrackedObject";
import {SchemeActions} from "../../Scheme/SchemeSidePopupActions";
import {ConceptActions} from "../../Concept/ConceptSidePopupActions";

const Search = loadable(() => import("./Search"));

const ConceptRelations = loadable(() => import("../../Concept/ConceptRelations"));
const ConceptProperties = loadable(() => import("../../Concept/ConceptProperties"));
const ConceptSidePopupActions = loadable(() => import("../../Concept/ConceptSidePopupActions"));

const SchemeProperties = loadable(() => import("../../Scheme/SchemeProperties"));
const SchemeSidePopupActions = loadable(() => import("../../Scheme/SchemeSidePopupActions"));

const classes = {
  root: {
    height: "100%",
    background: "white"
  },
  topBar: {
    position: "relative",
    flexBasis: 0
  },
  content: {
    flexGrow: 1,
    overflow: "auto",
    "& > .MuiGrid-item":{
      p: 2
    }
  },
  tabs: (theme) => ({
    width: `calc(100% - ${theme.spacing(8)}px)`
  }),
  toggleIcon: (theme) => ({
    position: "absolute",
    top: theme.spacing(2),
    right: theme.spacing(1)
  }),
  column: {
    padding: 2,
    overflow: "auto",
    height: "100%"
  },
  rightColumn: (theme) => ({
    borderLeft: `1px solid ${theme.palette.grey["400"]}`
  }),
  preview: (theme) => ({
    overflow: "auto",
    background: "white",
    zIndex: 1,
    paddingBottom: theme.spacing(2),
    "&:not(:first-of-type)": {
      zIndex: 2,
      borderTop: `1px solid ${theme.palette.grey["400"]}`,
      paddingTop: theme.spacing(2),
      marginTop: theme.spacing(-7),
    }
  })
};

const GenericActions = {
  VIEW_DETAILS: "VIEW_DETAILS",
  OPEN_NARROWERS: "OPEN_NARROWERS",
  CLOSE_NARROWERS: "CLOSE_NARROWERS",
};

const tabsIndices = {
  INFOS: "infos",
  ACTIONS: "actions",
  SEARCH: "search"
};


const tabsMapping = {
  [tabsIndices.INFOS] : [
    ...Object.values(GenericActions)
  ],
  [tabsIndices.ACTIONS] : [
    ...Object.values(SchemeActions),
    ...Object.values(ConceptActions),
    ...Object.values(VocabularyActions)
  ]
};

export function isSidePopupWide(actionType){
  return actionType && ![GenericActions.OPEN_NARROWERS, GenericActions.CLOSE_NARROWERS].includes(actionType);
}

/**
 *
 */
export const SidePopup = memo(
  ({searchQuery, sidePreviewedObjects = [], onClickAssociatedConcept, vocabulary, searchInVocabulary} = {}) => {

    const {t} = useTranslation();
    const [tabValue, setTabValue] = useState(tabsIndices.INFOS);
    const [trackedObject] = useTrackedObject();
    const [actionType, setActionType] = useTrackedAction();

    useEffect(() => {
      if (!!searchQuery) {
        setTabValue(tabsIndices.SEARCH);
      }
    }, [searchQuery]);

    useEffect(() => {
      const tabValue = Object.entries(tabsMapping).find(([, actions]) => actions.includes(actionType))?.[0] || tabsIndices.INFOS;
      setTabValue(tabValue);
    }, [trackedObject?.id, actionType]);

    if (trackedObject) {
      sidePreviewedObjects = [trackedObject, ...sidePreviewedObjects];
    }

    const wideOpen = isSidePopupWide(actionType);

    return (
      <Grid container direction={"column"} wrap={"nowrap"} sx={classes.root}>
        <Grid item sx={classes.topBar}>
          {renderTabs()}

          <IconButton onClick={handleToggleWideOpen} sx={classes.toggleIcon}>
            {wideOpen ?  <CloseIcon /> : <OpenIcon />}
          </IconButton>

          <Divider />
        </Grid>
        <Grid item container sx={classes.content} wrap={"nowrap"}>
          {renderContent()}
        </Grid>
      </Grid>
    );

    function renderTabs() {
      return (
        <Tabs
          value={tabValue}
          onChange={(_, tabValue) => {
            setTabValue(tabValue);
          }}
          variant="fullWidth"
          sx={classes.tabs}>
          <Tab
            value={tabsIndices.INFOS}
            icon={<InfoIcon />}
            label={!actionType ? t("VOCABULARY.SIDE_POPUP.TABS.PREVIEW") : t("VOCABULARY.SIDE_POPUP.TABS.INFORMATION")}
          />

          <If condition={trackedObject}>
            <Tab
              value={tabsIndices.ACTIONS}
              icon={<MenuOpenIcon />}
              label={t("VOCABULARY.SIDE_POPUP.TABS.CONTEXT_MENU")}
            />
          </If>

          <If condition={searchQuery}>
            <Tab value={tabsIndices.SEARCH} icon={<SearchIcon />} label={t("VOCABULARY.SIDE_POPUP.TABS.SEARCH")} />
          </If>
        </Tabs>
      );
    }

    function renderContent() {
      return (
        <Choose>
          <When condition={tabValue === tabsIndices.SEARCH}>
            <Grid item xs sx={classes.column}>
              <Search
                searchQuery={searchQuery}
                onClickConcept={onClickAssociatedConcept}
                vocabulary={searchInVocabulary}
              />
            </Grid>
          </When>

          <When condition={tabValue === tabsIndices.ACTIONS && trackedObject}>
            <Choose>
              <When condition={trackedObject.__typename === "Scheme"}>
                <SchemeSidePopupActions scheme={trackedObject} vocabulary={vocabulary} />
              </When>

              <When condition={trackedObject.__typename === "Concept"}>
                <ConceptSidePopupActions concept={trackedObject} vocabulary={vocabulary} />
              </When>

              <Otherwise>
                <VocabularySidePopupActions vocabulary={trackedObject} />
              </Otherwise>
            </Choose>
          </When>

          <When condition={trackedObject?.__typename === "Vocabulary"}>
            <Grid item xs={6} sx={classes.column}>
              <Typography variant={"h5"} gutterBottom>
                {trackedObject.prefLabel}
              </Typography>
              <Divider />
              <p>Tutoriel ici...</p>
            </Grid>
          </When>

          <When condition={actionType === GenericActions.VIEW_DETAILS}>
            <Grid item xs={6} sx={classes.column}>
              {renderObjectProperties(trackedObject)}
            </Grid>

            <Grid item xs={6} sx={[classes.column, classes.rightColumn]}>
              <ConceptRelations id={trackedObject.id} onClickAssociatedConcept={onClickAssociatedConcept} />
            </Grid>
          </When>

          <When condition={trackedObject}>
            <Grid item container direction={"column"} wrap={"nowrap"}>
              {sidePreviewedObjects.map((previewedObject) => (
                <Grid item key={previewedObject.id} xs sx={classes.preview}>
                  {renderObjectProperties(previewedObject)}
                </Grid>
              ))}
            </Grid>
          </When>
        </Choose>
      );
    }

    function renderObjectProperties(object) {
      if (object) {
        return object.__typename === "Scheme" ? (
          <SchemeProperties id={object.id} prefLabel={object.prefLabel} />
        ) : (
          <ConceptProperties id={object.id} prefLabel={object.prefLabel} />
        );
      }
    }

    function handleToggleWideOpen() {
      setActionType(actionType === GenericActions.VIEW_DETAILS ? null : GenericActions.VIEW_DETAILS);
    }
  },

  pickedPropsAreEquals(["searchQuery", "sidePreviewedObjects", "vocabulary.id", "searchInVocabulary.id"])
);
