import {memo} from "react";
import {string, func, object, bool} from "prop-types";
import {Box, Button as MuiButton, Grid, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {usePaginatedQuery} from "@synaptix/ui";
import {InfiniteScrollColumn} from "../../../widgets/Column/InfiniteScrollColumn";

import {gqlSchemes} from "../../Scheme/gql/Schemes.gql";
import {SchemeListItem} from "./SchemeListItem";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";
import {useTrackedAction} from "../../../../hooks/useTrackedAction";
import {VocabularyActions} from "../VocabularySidePopupActions";

const classes = {
  emptyMessage: (theme) => ({
    marginBottom: theme.spacing(6),
    color: theme.palette.text.hint
  })
};

export const VIRTUAL_DRAFT_SCHEME_ID = "VIRTUAL_DRAFT_SCHEME";

/**
 *
 */
export const SchemesColumn = memo(
  ({vocabularyId, expandedScheme, selectedScheme, onSelectScheme, sxClass, dense} = {}) => {

    const {t} = useTranslation();
    const [actionType, setActionType] = useTrackedAction();

    const {data: {schemes, draftConceptsCount} = {}, loading, loadNextPage} = usePaginatedQuery(
      gqlSchemes,
      {
        connectionPath: "schemes",
        pageSize: 50,
        variables: {
          filters: ["hasVocabulary:" + vocabularyId],
          draftConceptsFilters: ["hasVocabulary:" + vocabularyId, "inScheme != *"],
          sortings: [{sortBy: "prefLabel"}]
        }
      }
    );

    return schemes?.totalCount === 0 ? (
      <Grid container alignContent={"center"} justify={"center"} direction={"column"} wrap={"nowrap"}>
        <Grid item xs={12} container alignItems={"flex-end"} justify={"center"}>
          <Typography variant="h5" sx={classes.emptyMessage}>
            {t("VOCABULARY.EMPTY_MESSAGE")}
          </Typography>
        </Grid>
        <Grid item xs={12} container alignItems={"flex-start"} justify={"center"}>
          <MuiButton variant={"outlined"} onClick={handleCreateBranch}>
            {t("VOCABULARY.ACTIONS.ADD_SCHEME")}
          </MuiButton>
        </Grid>
      </Grid>
    ) : (
      <Box sx={sxClass}>
        <InfiniteScrollColumn
          dense={dense}
          loading={loading}
          hasNextPage={schemes?.pageInfo.hasNextPage}
          totalCount={schemes?.totalCount }
          onLoadNextPage={loadNextPage}>
          {(schemes?.edges || []).map(({node: scheme}) => (
            <SchemeListItem
              key={scheme.id}
              scheme={scheme}
              selected={selectedScheme?.id === scheme.id}
              expanded={scheme.topConceptsCount > 0 && expandedScheme?.id === scheme.id}
              onSelectScheme={onSelectScheme}
            />
          ))}

          <If condition={draftConceptsCount > 0}>
            <SchemeListItem
              scheme={{
                __typename: "Scheme",
                id: VIRTUAL_DRAFT_SCHEME_ID,
                prefLabel: t("SCHEME.VIRTUAL_DRAFT_SCHEME.LABEL"),
                topConceptsCount: draftConceptsCount,
                vocabularyId: vocabularyId
              }}
              selected={selectedScheme?.id === VIRTUAL_DRAFT_SCHEME_ID}
              expanded={expandedScheme?.id === VIRTUAL_DRAFT_SCHEME_ID}
              onSelectScheme={onSelectScheme}
            />
          </If>
        </InfiniteScrollColumn>
      </Box>
    );

    function handleCreateBranch() {
      setActionType(VocabularyActions.ADD_SCHEME);
    }
  },
  pickedPropsAreEquals(["vocabularyId", "expandedScheme.id", "selectedScheme.id", "sxClass", "dense"])
);

SchemesColumn.propTypes = {
  sxClass: object,
  vocabularyId: string.isRequired,
  onSelectScheme: func.isRequired,
  selectedScheme: object,
  expandedScheme: object,
  dense: bool
};
