
import {List, ListItemText, ListItem} from "@mui/material";
import {useTranslation} from "react-i18next";
import {ConceptListItem} from "./ConceptListItem";
import {usePinnedConcepts} from "../../../../hooks/usePinnedConcepts";

const classes = {
  root: {
    background: "white"
  }
};

/**
 * @param {object} sxClass
 * @param {function} onSelectConcept
 * @returns {JSX.Element}
 * @constructor
 */
export function PinnedConcepts({sxClass} = {}) {

  const {t} = useTranslation();
  const [pinnedConcepts, setPinnedConcepts] = usePinnedConcepts();

  return (
    <List dense sx={[classes.root, sxClass]}>
      {pinnedConcepts.map((concept) => (
        <ConceptListItem key={concept.id} concept={{...concept}} pinned />
      ))}
      <If condition={pinnedConcepts.length === 0}>
        <ListItem disabled>
          <ListItemText>{t("VOCABULARY.NO_PINNED_CONCEPT")}</ListItemText>
        </ListItem>
      </If>
    </List>
  );
}
