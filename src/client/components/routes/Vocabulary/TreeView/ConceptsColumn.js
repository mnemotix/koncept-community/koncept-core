import {memo, useEffect, useState} from "react";
import {string, func, object, number, array} from "prop-types";
import {InfiniteScrollColumn} from "../../../widgets/Column/InfiniteScrollColumn";
import {ListSubheader, Box} from "@mui/material";
import {usePaginatedQuery} from "@synaptix/ui";
import ScrollIntoViewIfNeeded from "react-scroll-into-view-if-needed";

import {gqlConcepts} from "../../Concept/gql/Concepts.gql";
import {ConceptListItem} from "./ConceptListItem";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";
import {VIRTUAL_DRAFT_SCHEME_ID} from "./SchemesColumn";

const classes = {
  moreItemsBeforeSelected: (theme) => ({
    margin: theme.spacing(2, 0)
  })
};

const pageSize = 30;

export function getConceptsColumnGqlVariables({broaderObject}){
  let filters = [];

  if (!broaderObject || broaderObject.id === VIRTUAL_DRAFT_SCHEME_ID) {
    filters = ["inScheme != *", `hasVocabulary: ${broaderObject.vocabularyId}`];
  } else if (broaderObject.__typename === "Scheme") {
    filters = [`topInScheme:${broaderObject.id}`];
  } else {
    filters = [`hasBroader:${broaderObject.id}`];
  }

  return {
    filters,
    sortings: [{sortBy: "prefLabel"}],
    first: pageSize
  }
}
/**
 *
 */
export const ConceptsColumn = memo(
  ({
    sxClass = {},
    broaderObject,
    onSelectConcept,
    expandedConcept,
    selectedConcept,
    dense,
    estimatedConceptsCount
  } = {}) => {
    const [expandedConceptMissingInData, setExpandedConceptMissingInData] = useState(false);

    const {data: {concepts} = {}, loading, loadNextPage} = usePaginatedQuery(gqlConcepts, {
      connectionPath: "concepts",
      pageSize,
      variables: getConceptsColumnGqlVariables({broaderObject})
    });

    useEffect(() => {
      if (concepts && expandedConcept) {
        setExpandedConceptMissingInData(!concepts.edges.find(({node}) => node.id === expandedConcept?.id));
      }
    }, [concepts, expandedConcept]);

    return loading || concepts?.totalCount > 0 ? (
      <Box sx={{...sxClass, WebkitScrollbar: "display: none"}}>
        <InfiniteScrollColumn
          dense={dense}
          hasNextPage={concepts?.pageInfo.hasNextPage}
          loading={loading}
          onLoadNextPage={loadNextPage}
          totalCount={concepts?.totalCount || estimatedConceptsCount }>
          {(concepts?.edges || []).map(({node: concept}, index) =>
            renderConceptItem({
              concept,
              key: concept.id + index
            })
          )}

          <If condition={!loading && expandedConceptMissingInData && expandedConcept}>
            <ListSubheader disableSticky sx={classes.moreItemsBeforeSelected}>
              ...
            </ListSubheader>
            {renderConceptItem({
              concept: expandedConcept
            })}
            <ListSubheader disableSticky>...</ListSubheader>
          </If>
        </InfiniteScrollColumn>
      </Box>
    ) : null;

    function renderConceptItem({concept, key}) {
      return (
        <ScrollIntoViewIfNeeded
          key={key || concept.id}
          active={selectedConcept?.id === concept.id}
          options={{
            behavior: "smooth",
            block: "end",
            inline: "end"
          }}>
          <ConceptListItem
            concept={concept}
            expanded={concept.narrowerConceptsCount > 0 && expandedConcept?.id === concept.id}
            selected={selectedConcept?.id === concept.id}
            onSelectConcept={onSelectConcept}
          />
        </ScrollIntoViewIfNeeded>
      );
    }
  },
  pickedPropsAreEquals(["broaderObject.id", "expandedConcept", "selectedConcept", "estimatedConceptsCount"])
);

ConceptsColumn.propTypes = {
  className: string,
  broaderObject: object.isRequired,
  onSelectConcept: func.isRequired,
  selectedConcept: object,
  expandedConcept: object,
  estimatedConceptsCount: number
};
