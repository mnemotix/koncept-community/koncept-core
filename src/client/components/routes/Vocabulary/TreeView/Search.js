import {Fragment, memo} from "react";
import {List, ListItem, ListSubheader, ListItemText, Box} from "@mui/material";
import Skeleton from '@mui/material/Skeleton';
import {useTranslation} from "react-i18next";
import {usePaginatedQuery} from "@synaptix/ui";
import {ConceptListItem} from "./ConceptListItem";

import {gqlConcepts} from "../../Concept/gql/Concepts.gql";
import {pickedPropsAreEquals} from "../../../../utilities/pickedPropsAreEquals";

const classes = {
  schemeItem: {
    paddingLeft: 4
  },
  conceptItem: {
    paddingLeft: 8
  },
  noMatch: {
    textAlign: "center",
    color: "text.emptyHint"
  }
};

/**
 *
 */
const Search = memo(({searchQuery, onClickConcept, vocabulary} = {}) => {

  const {t} = useTranslation();

  const {data: {concepts} = {}, loading, loadNextPage} = usePaginatedQuery(gqlConcepts, {
    connectionPath: "concepts",
    pageSize: 50,
    variables: {
      qs: searchQuery,
      first: 50,
      filters: vocabulary?.id ? [`hasVocabulary:${vocabulary.id}`] : null
    }
  });

  const vocabularies = orderConcepts(concepts);

  return (
    <List dense>
      <If condition={loading}>
        <ListSubheader disableSticky>
          <Skeleton width={"50%"} />
        </ListSubheader>
        <List component="div" disablePadding>
          <ListSubheader disableSticky sx={classes.schemeItem}>
            <Skeleton width={"50%"} />
          </ListSubheader>
          <List component="div" disablePadding>
            {[...Array(10)].map((_, index) => {
              return (
                <ListItem key={index}>
                  <ListItemText
                    sx={classes.conceptItem}
                    primary={<Skeleton />}
                    secondary={<Skeleton width={"50%"} />}
                  />
                </ListItem>
              );
            })}
          </List>
        </List>
      </If>
      <If condition={concepts?.totalCount === 0}>
        <Box sx={classes.noMatch}>
          {t("SEARCH.NO_MATCH")}
        </Box>
      </If>
      {vocabularies.map(({vocabularyPrefLabel, schemes = {}} = {}, index) => (
        <Fragment key={index}>
          <ListSubheader disableSticky>{vocabularyPrefLabel}</ListSubheader>
          {Object.values(schemes).map(({schemePrefLabel, concepts = []} = {}, index) => (
            <List key={index} component="div" disablePadding>
              <ListSubheader disableSticky sx={classes.schemeItem}>
                {schemePrefLabel}
              </ListSubheader>
              <List component="div" disablePadding>
                {concepts.map((concept) => (
                  <ConceptListItem
                    key={concept.id}
                    sxClass={classes.conceptItem}
                    concept={concept}
                    onSelectConcept={onClickConcept}
                  />
                ))}
              </List>
            </List>
          ))}
        </Fragment>
      ))}
    </List>
  );

  function orderConcepts(concepts) {
    return Object.values(
      (concepts?.edges || []).reduce((acc, {node: concept}) => {
        if (!concept.vocabularyPrefLabel) {
          return acc;
        }

        if (!acc[concept.vocabularyPrefLabel]) {
          acc[concept.vocabularyPrefLabel] = {
            vocabularyPrefLabel: concept.vocabularyPrefLabel,
            schemes: {}
          };
        }

        if (!acc[concept.vocabularyPrefLabel].schemes[concept.schemePrefLabel]) {
          acc[concept.vocabularyPrefLabel].schemes[concept.schemePrefLabel] = {
            schemePrefLabel: concept.schemePrefLabel,
            concepts: []
          };
        }

        acc[concept.vocabularyPrefLabel].schemes[concept.schemePrefLabel].concepts.push(concept);

        return acc;
      }, {})
    );
  }
}, pickedPropsAreEquals(["searchQuery", "vocabulary.id"]));

export default Search;
