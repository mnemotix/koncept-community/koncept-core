import {renderLinkComponent} from "@synaptix/ui";
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Typography,
  Button as MuiButton,
  Grid,
  Dialog,
  DialogContent,
  DialogTitle
} from "@mui/material";
import {useHistory} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {AddBoxOutlined as AddIcon, SystemUpdateAlt as ImportIcon} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {gqlVocabularies} from "./gql/Vocabularies.gql";
import {Progress, ROUTES} from "../../..";
import {useEffect, useState} from "react";
import {VocabularyEdit} from "./VocabularyEdit";

const classes = {
  creationPanel: {
    marginTop: 4
  }
};

/**
 *
 */
export function VocabulariesList({sxClass, onSelectVocabulary, creationDisabled = false} = {}) {

  const {t} = useTranslation();
  const history = useHistory();
  const [createDialogOpen, setCreateDialogOpen] = useState(false);

  const [getVocabularies, {data: {vocabularies} = {}, loading}] = useLazyQuery(gqlVocabularies);

  useEffect(() => {
    getVocabularies();
  }, []);

  return loading ? (
    <Progress />
  ) : (
    <>
      <Grid container>
        <Grid item xs={12}>
          <List sx={sxClass}>
            {(vocabularies?.edges || []).map(({node: vocabulary}) => {
              const {id, prefLabel, altLabel, conceptsCount} = vocabulary;
              let listItemProps = {};

              if (onSelectVocabulary) {
                listItemProps.onClick = onSelectVocabulary.bind(this, vocabulary);
              } else {
                listItemProps.component = renderLinkComponent({
                  to: ROUTES.VOCABULARY,
                  params: {id}
                });
              }

              return (
                <ListItem key={id} {...listItemProps}>
                  <ListItemText primary={altLabel} secondary={prefLabel} />
                  <ListItemSecondaryAction>
                    <Typography variant={"subtitle1"}>
                      {t("VOCABULARY.CONCEPTS_COUNT", {count: conceptsCount})}
                    </Typography>
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}

            <If condition={vocabularies?.edges.length === 0}>
              <ListItem disabled>
                <ListItemText primary={t("HOME.VOCABULARIES.EMPTY")} />
              </ListItem>
            </If>
          </List>
        </Grid>
        <If condition={!creationDisabled}>
          <Grid item xs={12} container sx={classes.creationPanel}>
            <Grid item xs={6}>
              <MuiButton
                variant={"outlined"}
                startIcon={<AddIcon />}
                onClick={() => setCreateDialogOpen(true)}>
                {t("HOME.VOCABULARIES.CREATE")}
              </MuiButton>
            </Grid>
            <Grid item xs={6}>
              <MuiButton variant={"outlined"} disabled startIcon={<ImportIcon />}>
                {t("HOME.VOCABULARIES.IMPORT")}
              </MuiButton>
            </Grid>
          </Grid>
        </If>
      </Grid>
      <If condition={!creationDisabled}>
        <Dialog open={createDialogOpen} onClose={() => setCreateDialogOpen(false)} fullWidth={true} maxWidth={"md"}>
          <DialogTitle>{t("VOCABULARY.CREATE")}</DialogTitle>
          <DialogContent>
            <VocabularyEdit
              onSuccess={(vocabulary) => {
                getVocabularies();
                history.push(formatRoute(ROUTES.VOCABULARY, {id: vocabulary.id}));
              }}
            />
          </DialogContent>
        </Dialog>
      </If>
    </>
  );
}
