import {useState, useMemo, Fragment} from "react";
import {Button} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {LinkInputDefinition} from "@synaptix/ui";
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListSubheader,
  Checkbox,
  Grid, Box,
  Button as MuiButton,
  IconButton,
  ListItemSecondaryAction
} from "@mui/material";
import {Delete as DeleteIcon} from "@mui/icons-material";
import {usePinnedConcepts} from "../../../../hooks/usePinnedConcepts";

import {useSnackbar} from "notistack";
import {gqlUpdateConcept} from "../../Concept/gql/UpdateConcept.gql";
import {useMutation} from "@apollo/client";

const popupBorder = "2px solid rgba(0, 0, 0, 0.23)";
const innerBorder = "1px solid rgba(0, 0, 0, 0.10)";

const classes = {
  root: {
    height: "100%"
  },
  viewport: {
    height: "100%",
    overflow: "hidden"
  },
  header: {
    flexBasis: 0
  },
  popupTitle: (theme) => ({
    border: popupBorder,
    borderBottom: 0,
    padding: theme.spacing(2),
    marginLeft: -2,
    fontSize: "2rem",
    color: theme.palette.secondary.main,
    textTransform: "uppercase",
    [theme.breakpoints.down("lg")]: {
      padding: theme.spacing(1),
      fontSize: "1.5rem"
    },
    display: "flex",
    alignItems: "center"
  }),
  closeButton: {
    padding: 0
  },
  column: (theme) => ({
    padding: theme.spacing(2),
    borderTop: innerBorder,
    "&:not(:last-of-type)": {
      borderRight: innerBorder
    },
    height: "100%",
    overflow: "hidden"
  }),
  columnTitle: (theme) => ({
    flexBasis: 0,
    fontSize: "1.75rem",
    borderBottom: innerBorder,
    paddingBottom: theme.spacing(2),
    [theme.breakpoints.down("lg")]: {
      fontSize: "1.5rem",
      paddingBottom: theme.spacing(1)
    }
  }),
  columnContent: {
    overflow: "auto"
  },
  columnSubTitle: {
    fontSize: "1.1rem",
    borderBottom: innerBorder,
    marginBottom: 2,
    marginTop: 2,
    color: "secondary.main"
  },
  sourceConceptColumn: {},
  conceptPrimaryLabel: {
    cursor: "pointer"
  },
  selected: {
    color: "secondary.main"
  },
  relationPrimaryLabel: {
    textTransform: "uppercase",
    fontWeight: "bold"
  },
  relationPrimaryLabelEnhanced: (theme) => ({
    textTransform: "uppercase",
    fontWeight: "bold",
    color: theme.palette.secondary.main,
    margin: theme.spacing(0, 1)
  }),
  noSourceConceptTip: {
    color: "text.emptyHint",
    padding: 10,
    textAlign: "center",
    fontSize: "1.1rem"
  },
  addRelationsContainer: (theme) => ({
    textAlign: "center",
    padding: theme.spacing(4, 0),
    borderTop: innerBorder,
    maxHeight: "50%",
    overflow: "auto"
  })
};

const semanticRelations = [
  {
    i18n: "NARROWER",
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerConcepts",
      inputName: "narrowerConceptInputs",
      isPlural: true
    }),
    isSemanticRelation: true
  },
  {
    i18n: "NARROWER_TRANSITIVE",
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerTransitiveConcepts",
      inputName: "narrowerTransitiveConceptInputs",
      isPlural: true
    }),
    isSemanticRelation: true
  },
  {
    i18n: "RELATED",
    linkInputDefinition: new LinkInputDefinition({
      name: "relatedConcepts",
      inputName: "relatedConceptInputs",
      isPlural: true
    }),
    isSemanticRelation: true
  }
];

const isMappingRelations = [
  {
    i18n: "NARROWER_MATCH",
    linkInputDefinition: new LinkInputDefinition({
      name: "narrowerMatchConcepts",
      inputName: "narrowerMatchInputs",
      isPlural: true
    }),
    isMappingRelation: true
  },
  {
    i18n: "RELATED_MATCH",
    linkInputDefinition: new LinkInputDefinition({
      name: "relatedMatchConcepts",
      inputName: "relatedMatchConceptInputs",
      isPlural: true
    }),
    isMappingRelation: true
  },
  {
    i18n: "EXACT_MATCH",
    linkInputDefinition: new LinkInputDefinition({
      name: "exactMatchConcepts",
      inputName: "exactMatchConceptInputs",
      isPlural: true
    }),
    isMappingRelation: true
  },
  {
    i18n: "CLOSE_MATCH",
    linkInputDefinition: new LinkInputDefinition({
      name: "closeMatchConcepts",
      inputName: "closeMatchConceptInputs",
      isPlural: true
    }),
    isMappingRelation: true
  }
];

/**
 *
 */
export function Relations({onClose, selectedConcept, onSelectConcept} = {}) {
  const {t} = useTranslation();
  const [pinnedConcepts] = usePinnedConcepts();
  const [sourceConcept, setSourceConcept] = useState(selectedConcept);
  const [targetRelation, setTargetRelation] = useState();
  const [targetConcepts, setTargetConcepts] = useState([]);
  const [pendingRelations, setPendingRelations] = useState([]);
  const {enqueueSnackbar} = useSnackbar();

  const orderedPinnedConcepts = useMemo(() => {
    return orderPinnedConcepts(pinnedConcepts);
  }, [pinnedConcepts.length]);

  const [mutateConcept, {loading: saving}] = useMutation(gqlUpdateConcept);

  return (
    <Grid container sx={classes.root} direction="column" wrap={"nowrap"}>
      <Grid xs item container sx={classes.viewport}>
        <Grid
          container
          item
          xs={4}
          sx={[classes.column, classes.sourceConceptColumn]}
          direction={"column"}
          wrap={"nowrap"}>
          <Grid item sx={classes.columnTitle}>
            {t("RELATIONS.SOURCE_CONCEPT")}
          </Grid>

          <Grid item xs sx={classes.columnContent}>
            <If condition={selectedConcept && !pinnedConcepts.some(({id}) => id === selectedConcept.id)}>
              <Box sx={classes.columnSubTitle}>{t("RELATIONS.SELECTED_CONCEPT")}</Box>

              <ListSubheader disableSticky>{selectedConcept.vocabularyPrefLabel}</ListSubheader>

              {renderConcept({
                concept: selectedConcept,
                checked: sourceConcept?.id === selectedConcept.id,
                onSelect: () => handleSelectSourceConcept({concept: selectedConcept})
              })}
            </If>

            <If condition={pinnedConcepts?.length > 0}>
              <Box sx={classes.columnSubTitle}>{t("RELATIONS.PINNED_CONCEPTS")}</Box>

              {renderSourcePinnedConcepts()}
            </If>

            <If condition={!selectedConcept && pinnedConcepts?.length === 0}>
              <Box sx={classes.noSourceConceptTip}>{t("RELATIONS.NO_SOURCE_CONCEPT_TIP")}</Box>
            </If>
          </Grid>
        </Grid>
        <Grid container item xs={4} sx={classes.column} direction={"column"} wrap={"nowrap"}>
          <Grid item sx={classes.columnTitle}>
            {t("RELATIONS.RELATION")}
          </Grid>

          <Grid item xs sx={classes.columnContent}>
            <Box sx={classes.columnSubTitle}>{t("RELATIONS.INNER_ASSOCIATIONS")}</Box>

            {semanticRelations.map((relation) =>
              renderRelation({
                relation,
                checked: targetRelation === relation,
                onSelect: () => handleSelectRelation({relation}),
                disabled:
                  sourceConcept &&
                  targetConcepts.some(
                    ({vocabularyPrefLabel}) => vocabularyPrefLabel !== sourceConcept.vocabularyPrefLabel
                  )
              })
            )}

            <Box sx={classes.columnSubTitle}>{t("RELATIONS.OUTER_ASSOCIATIONS")}</Box>

            {isMappingRelations.map((relation) =>
              renderRelation({
                relation,
                checked: targetRelation === relation,
                onSelect: () => handleSelectRelation({relation}),
                disabled:
                  sourceConcept &&
                  targetConcepts.some(
                    ({vocabularyPrefLabel}) => vocabularyPrefLabel === sourceConcept.vocabularyPrefLabel
                  )
              })
            )}
          </Grid>
        </Grid>
        <Grid container item xs={4} sx={classes.column} direction={"column"} wrap={"nowrap"}>
          <Grid sx={classes.columnTitle}>{t("RELATIONS.TARGET_CONCEPTS")}</Grid>

          <Grid item xs sx={classes.columnContent}>
            <If condition={pinnedConcepts?.length > 0}>
              <Box sx={classes.columnSubTitle}>{t("RELATIONS.PINNED_CONCEPTS")}</Box>
              {renderTargetPinnedConcepts()}
            </If>

            <If condition={pinnedConcepts?.length === 0}>
              <Box sx={classes.noSourceConceptTip}>{t("RELATIONS.NO_SOURCE_CONCEPT_TIP")}</Box>
            </If>
          </Grid>
          <If condition={pendingRelations.length > 0 || (sourceConcept && targetRelation && targetConcepts.length > 0)}>
            <Grid item sx={classes.addRelationsContainer}>
              <Choose>
                <When condition={sourceConcept && targetRelation && targetConcepts.length > 0}>
                  <MuiButton color="secondary" size="large" variant="contained" onClick={handleAddPendingRelations}>
                    {t("RELATIONS.ADD_RELATIONS", {count: targetConcepts.length})}
                  </MuiButton>
                </When>
                <Otherwise>
                  <List>
                    {pendingRelations.map((pendingRelation, index) => renderPendingRelation({pendingRelation, index}))}
                  </List>
                  <Button
                    loading={saving}
                    color="secondary"
                    size="large"
                    variant="contained"
                    onClick={handleSavePendingRelations}>
                    {t("RELATIONS.SAVE_RELATIONS")}
                  </Button>
                </Otherwise>
              </Choose>
            </Grid>
          </If>
        </Grid>
      </Grid>
    </Grid>
  );

  function orderPinnedConcepts() {
    return Object.values(
      pinnedConcepts.reduce((acc, concept) => {
        if (!acc[concept.vocabularyPrefLabel]) {
          acc[concept.vocabularyPrefLabel] = {
            vocabularyPrefLabel: concept.vocabularyPrefLabel,
            concepts: []
          };
        }

        acc[concept.vocabularyPrefLabel].concepts.push(concept);

        return acc;
      }, {})
    );
  }

  function renderSourcePinnedConcepts() {
    return orderedPinnedConcepts.map(({vocabularyPrefLabel, concepts}) => (
      <Fragment key={vocabularyPrefLabel}>
        <ListSubheader disableSticky>{vocabularyPrefLabel}</ListSubheader>
        {concepts.map((concept) => {
          // A source concept is disabled if
          //  - one of target concept is the same one.
          //  - relation type is a mapping relation and one of target concept is in same vocabulary
          //  - relation type is a semantic relation and one of target concept is not in same vocabulary
          const disabled =
            targetConcepts.some(({id}) => id === concept.id) ||
            (targetRelation?.isSemanticRelation &&
              targetConcepts.some(({vocabularyPrefLabel}) => vocabularyPrefLabel !== concept.vocabularyPrefLabel)) ||
            (targetRelation?.isMappingRelation &&
              targetConcepts.some(({vocabularyPrefLabel}) => vocabularyPrefLabel === concept.vocabularyPrefLabel));

          return renderConcept({
            concept,
            checked: sourceConcept?.id === concept.id,
            onSelect: () => handleSelectSourceConcept({concept}),
            disabled
          });
        })}
      </Fragment>
    ));
  }

  function renderTargetPinnedConcepts() {
    return orderedPinnedConcepts.map(({vocabularyPrefLabel, concepts}) => (
      <Fragment key={vocabularyPrefLabel}>
        <ListSubheader disableSticky>{vocabularyPrefLabel}</ListSubheader>
        {concepts.map((concept) => {
          const checked = targetConcepts.some(({id}) => id === concept.id);
          // A target concept is disabled if
          //  - source concept is the same one.
          //  - relation type is a mapping relation and target concept is in same vocabulary
          //  - relation type is a semantic relation and target concept is not in same vocabulary
          const disabled =
            sourceConcept?.id === concept.id ||
            (targetRelation?.isSemanticRelation &&
              sourceConcept?.vocabularyPrefLabel !== concept.vocabularyPrefLabel) ||
            (targetRelation?.isMappingRelation && sourceConcept?.vocabularyPrefLabel === concept.vocabularyPrefLabel);
          return renderConcept({
            concept,
            disabled,
            checked,
            onSelect: () => handleSelectTargetConcept({concept})
          });
        })}
      </Fragment>
    ));
  }

  function renderConcept({concept, checked, disabled, onSelect}) {
    return (
      <ListItem key={concept.id} disabled={disabled} dense>
        <ListItemIcon>
          <Checkbox edge="start" checked={checked} onChange={onSelect} />
        </ListItemIcon>
        <ListItemText
          onClick={() => !disabled && onSelectConcept({concept})}
          primary={concept.prefLabel}
          sx={{
            primary: [classes.conceptPrimaryLabel, selectedConcept?.id === concept.id && classes.selected]
          }}
        />
      </ListItem>
    );
  }

  function renderRelation({relation, checked, disabled, onSelect}) {
    return (
      <ListItem key={relation.i18n} disabled={disabled} dense>
        <ListItemIcon>
          <Checkbox disabled={disabled} edge="start" checked={checked} onChange={onSelect} />
        </ListItemIcon>
        <ListItemText
          sx={{primary: classes.relationPrimaryLabel}}
          primary={t(`RELATIONS.RELATION_TYPES.${relation.i18n}.TITLE`)}
        />
      </ListItem>
    );
  }

  function renderPendingRelation({pendingRelation, index}) {
    const [sourceConcept, targetRelation, targetConcept] = pendingRelation;

    return (
      <ListItem key={`${sourceConcept.id}.${targetRelation.i18n}.${targetConcept.id}`} dense>
        <ListItemText
          primary={
            <>
              <span>{sourceConcept.prefLabel}</span>
              <Box component="span" sx={classes.relationPrimaryLabelEnhanced}>
                {t(`RELATIONS.RELATION_TYPES.${targetRelation.i18n}.TITLE`)}
              </Box>
              <span>{targetConcept.prefLabel}</span>
            </>
          }
        />
        <ListItemSecondaryAction>
          <IconButton edge="end" onClick={() => handleRemovePendingRelation({pendingRelation, index})}>
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    );
  }

  function handleSelectSourceConcept({concept}) {
    const mutatedTargetConcepts = [...targetConcepts];
    const indexOf = mutatedTargetConcepts.findIndex(({id}) => concept.id === id);

    if (indexOf >= 0) {
      mutatedTargetConcepts.splice(indexOf, 1);
      setTargetConcepts(mutatedTargetConcepts);
    }

    if (sourceConcept === concept) {
      setSourceConcept(null);
    } else {
      setSourceConcept(concept);
    }
  }

  function handleSelectTargetConcept({concept}) {
    const mutatedTargetConcepts = [...targetConcepts];
    const indexOf = mutatedTargetConcepts.findIndex(({id}) => concept.id === id);

    if (indexOf === -1) {
      mutatedTargetConcepts.push(concept);
    } else {
      mutatedTargetConcepts.splice(indexOf, 1);
    }

    setTargetConcepts(mutatedTargetConcepts);
  }

  function handleSelectRelation({relation}) {
    if (relation === targetRelation) {
      setTargetRelation(null);
    } else {
      setTargetRelation(relation);
    }
  }

  function handleAddPendingRelations() {
    setPendingRelations([
      ...pendingRelations,
      ...targetConcepts.map((targetConcept) => [{...sourceConcept}, {...targetRelation}, {...targetConcept}])
    ]);
    setTargetConcepts([]);
    setTargetRelation(null);
  }

  function handleRemovePendingRelation({pendingRelation}) {
    setPendingRelations(pendingRelations.filter((relation) => relation !== pendingRelation));
  }

  async function handleSavePendingRelations() {
    const mutationInputs = pendingRelations.reduce((mutationInputs, [sourceConcept, targetRelation, targetConcept]) => {
      if (!mutationInputs[sourceConcept.id]) {
        mutationInputs[sourceConcept.id] = {
          objectId: sourceConcept.id,
          objectInput: {}
        };
      }

      const inputName = targetRelation.linkInputDefinition.inputName;

      if (!mutationInputs[sourceConcept.id].objectInput[inputName]) {
        mutationInputs[sourceConcept.id].objectInput[inputName] = [];
      }

      mutationInputs[sourceConcept.id].objectInput[inputName].push({
        id: targetConcept.id
      });

      return mutationInputs;
    }, {});

    for (let mutationInput of Object.values(mutationInputs)) {
      await mutateConcept({
        variables: {
          input: mutationInput
        }
      });
    }

    enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    setPendingRelations([]);
    setSourceConcept(null);
  }
}
