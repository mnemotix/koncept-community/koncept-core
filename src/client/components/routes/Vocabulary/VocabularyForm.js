import {useTranslation} from "react-i18next";
import {Tooltip, Box} from "@mui/material";
import {TextField} from "@synaptix/ui";
import {vocabularyProperties} from "./form/Vocabulary.form";

/**
 *
 */
export function VocabularyForm({ } = {}) {

  const {t} = useTranslation();

  return vocabularyProperties.map(({key, i18n, toolTip}, index) => (
    <Box key={key} sx={{marginBottom: 2}}>
      <Tooltip title={toolTip} placement="left">
          <TextField name={key} label={t("VOCABULARY.PROPERTIES." + i18n)} autoFocus={index === 0} />
      </Tooltip>
    </Box>
  ));
}
