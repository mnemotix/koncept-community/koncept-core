import {useEffect} from "react";
import {DynamicForm} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";
import {useSnackbar} from "notistack";
import Skeleton from '@mui/material/Skeleton';
import {vocabularyFormDefinition} from "./form/Vocabulary.form";
import {gqlUpdateVocabulary} from "./gql/UpdateVocabulary.gql";
import {gqlCreateVocabulary} from "./gql/CreateVocabulary.gql";
import {VocabularyForm} from "./VocabularyForm";
import {gqlVocabulary} from "./gql/Vocabulary.gql";


/**
 *
 */
export const VocabularyEdit = ({id, onSuccess} = {}) => {

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  const [getVocabulary, {data: {vocabulary} = {}, loading}] = useLazyQuery(gqlVocabulary);

  useEffect(() => {
    if (id) {
      getVocabulary({
        variables: {id}
      });
    }
  }, [id]);

  const [mutateVocabulary, {loading: saving}] = useMutation(id ? gqlUpdateVocabulary : gqlCreateVocabulary, {
    onCompleted: (data) => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      if (onSuccess) {
        onSuccess(id ? data.updateVocabulary.updatedObject : data.createVocabulary.createdObject);
      }
    }
  });

  return (
    <DynamicForm
      singleColumnLayout
      object={vocabulary}
      formDefinition={vocabularyFormDefinition}
      mutateFunction={mutateVocabulary}
      saving={saving}
    >
      <Choose>
        <When condition={id && loading}>
          <Skeleton variant="text" />
        </When>
        <Otherwise>
          <VocabularyForm />
        </Otherwise>
      </Choose>
    </DynamicForm>
  );
};
