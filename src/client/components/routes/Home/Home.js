import {useLoggedUser, useExtensionFragment} from "@synaptix/ui";
import {Typography, Grid, Divider, Box} from "@mui/material";

import {
  ListAltOutlined as ListIcon,
} from "@mui/icons-material";
import {useTranslation} from "react-i18next";

import {VocabulariesList} from "../Vocabulary/VocabulariesList";

const classes = {
  // Classes
  root: (theme) => ({
    height: "100%",
    padding: theme.spacing(2, 0)
  }),
  titleContainer: (theme) => ({
    padding: theme.spacing(0, 4)
  }),
  title: {
    fontSize: "2rem"
  },
  vocabulariesList: (theme) => ({
    maxHeight: "40vh",
    maxWidth: theme.spacing(100),
    overflow: "auto"
  }),
  content: {
    margin: "auto",
    marginTop: 2,
    flexGrow: 1
  },
  welcomeContainer: (theme) => ({
    border: `solid 1px ${theme.palette.grey["400"]}`,
    backgroundColor: "white",
    padding: theme.spacing(4),
    boxShadow: theme.shadows[0],
    zIndex: 2,
    width: "100%"
  }),
  vocabulariesListContainer: (theme) => ({
    border: `solid 1px ${theme.palette.grey["400"]}`,
    backgroundColor: "white",
    padding: theme.spacing(4),
    marginLeft: theme.spacing(-1),
    zIndex: 0
  }),
  welcome: {
    fontSize: "3rem"
  },
  welcomeMessage: {
    fontSize: "2rem"
  },
  actionIcon: {
    verticalAlign: "middle",
    marginRight: 1
  }
};

/**
 *
 */
export default function Home({ } = {}) {

  const {t} = useTranslation();
  const {user} = useLoggedUser();
  const HomeWelcomeExtension = useExtensionFragment("HomeWelcomeMessage");

  return (
    <Grid container sx={classes.root} direction="column" wrap={"nowrap"}>
      <Grid item style={{flexBasis: 0}} sx={classes.titleContainer}>
        <Typography variant={"h2"} gutterBottom sx={classes.title}>
          {t("HOME.TITLE")}
        </Typography>
        <Divider />
      </Grid>
      <Grid container item spacing={0} alignContent={"stretch"} sx={classes.content} >
        <Grid item container xs={5} sx={classes.welcomeContainer} id="bb">
          <Grid item style={{alignSelf: "center", width: "100%"}}>
            <Typography variant={"h1"} sx={classes.welcome}>
              {t("HOME.WELCOME", {name: user?.firstName})}
            </Typography>
            <Typography variant={"h2"} sx={classes.welcomeMessage}>
              {t("HOME.WELCOME_PHRASE")}
            </Typography>

            {HomeWelcomeExtension}
          </Grid>
        </Grid>

        <Grid id="cc" item xs={7} container direction="column" alignContent={"stretch"} justify="space-evenly"
          sx={{height: "100%", display: "flex", justifyContent: "center", }}>

          <Grid item sx={classes.vocabulariesListContainer}>
            <Typography variant={"h4"}>
              <ListIcon sx={classes.actionIcon} />
              {t("HOME.VOCABULARIES.VISUALIZE_TITLE")}
            </Typography>
            <VocabulariesList sxClass={classes.vocabulariesList} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
