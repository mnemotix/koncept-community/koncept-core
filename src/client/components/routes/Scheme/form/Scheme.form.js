/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlSchemeFragment} from "../gql/Scheme.gql";
import {ColorPickerField, TextAreaField} from "@synaptix/ui";

export const schemeProperties = [
  {
    key: "prefLabel",
    i18n: "PREF_LABEL",
    toolTip: "skos:prefLabel"
  },
  {
    key: "altLabel",
    i18n: "ALT_LABEL",
    toolTip: "skos:altLabel"
  },
  {
    key: "definition",
    i18n: "DEFINITION",
    toolTip: "skos:definition",
    FieldComponent: TextAreaField
  },
  {
    key: "color",
    i18n: "COLOR",
    toolTip: "mnx:color",
    FieldComponent: ColorPickerField
  }
];

export function getSchemeValidationSchema({t}) {
  return object().shape({
    prefLabel: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))
  });
}

export const schemeFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: [...schemeProperties.map(({key}) => key)],
    gqlFragment: gqlSchemeFragment,
    gqlFragmentName: "SchemeFragment"
  }),
  validationSchema: getSchemeValidationSchema
});
