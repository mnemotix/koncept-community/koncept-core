import {useTranslation} from "react-i18next";
import {Tooltip, Box} from "@mui/material";
import {TextField} from "@synaptix/ui";

import {schemeProperties} from "./form/Scheme.form";


/**
 *
 */
export function SchemeForm({ } = {}) {

  const {t} = useTranslation();

  return schemeProperties.map(({key, i18n, toolTip, FieldComponent = TextField}, index) => (
    <Box key={key} sx={{marginBottom: 2}}>
      <Tooltip title={toolTip} placement="left">
        <Box>
          <FieldComponent name={key} label={t("SCHEME.PROPERTIES." + i18n)} autoFocus={index === 0} />
        </Box>
      </Tooltip>
    </Box>
  ));
}
