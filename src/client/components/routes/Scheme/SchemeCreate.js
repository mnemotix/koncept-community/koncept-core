import {DynamicForm} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useMutation} from "@apollo/client";
import {useSnackbar} from "notistack";
import {schemeFormDefinition} from "./form/Scheme.form";
import {gqlCreateScheme} from "./gql/CreateScheme.gql";
import {SchemeForm} from "./SchemeForm";



/**
 * @param {object} vocabulary
 * @returns {JSX.Element}
 * @constructor
 */
export const SchemeCreate = ({vocabulary} = {}) => {
  if (!vocabulary) {
    throw new Error("A vocabulary must be passed");
  }

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  const [mutateScheme, {loading: saving}] = useMutation(gqlCreateScheme, {
    onCompleted: (data) => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });

  return (
    <DynamicForm
      singleColumnLayout
      formDefinition={schemeFormDefinition}
      mutateFunction={mutateScheme}
      mutateFunctionExtraParams={{
        refetchQueries: ["Schemes"]
      }}
      saving={saving}
      formatMutationInput={({objectInput}) => ({
        objectInput: {
          ...objectInput,
          vocabularyInputs: [
            {
              id: vocabulary.id
            }
          ]
        }
      })}>
      <SchemeForm />
    </DynamicForm>
  );
};
