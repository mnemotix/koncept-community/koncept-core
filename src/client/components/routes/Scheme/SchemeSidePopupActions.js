import {useEffect, useState} from "react";

import {useExtensionSettings} from "@synaptix/ui";
import {Grid, MenuList, MenuItem, ListItemIcon, Typography, Divider} from "@mui/material";
import {useTranslation} from "react-i18next";
import {
  ArrowLeft as UnfoldIcon,
  ArrowRight as FoldIcon,
  MenuOpen as MenuOpenIcon,
  Visibility as OpenIcon,
  Edit as EditIcon,
  Add as AddIcon,
  Delete as RemoveIcon
} from "@mui/icons-material";

import {SchemeEdit} from "./SchemeEdit";
import {useTrackedAction} from "../../../hooks/useTrackedAction";

import {ConceptCreate} from "../Concept/ConceptCreate";
import {ObjectRemovePanel} from "../../widgets/ObjectRemove/ObjectRemovePanel";


export const SchemeActions = {
  OPEN_NARROWERS: "OPEN_NARROWERS",
  CLOSE_NARROWERS: "CLOSE_NARROWERS",
  VIEW_DETAILS: "VIEW_DETAILS",
  SHOW_CONTEXT_MENU: "SHOW_CONTEXT_MENU",
  REMOVE: "REMOVE",
  EDIT: "EDIT",
  ADD_CHILDREN: "ADD_CHILDREN"
};

export function generateSchemeActions({t, expanded, scheme} = {}) {
  const {extendSchemeActions} = useExtensionSettings("SchemeActions");

  let actions = [
    {
      key: "OPEN_NARROWERS",
      label: t("SCHEME.ACTIONS.OPEN_NARROWERS"),
      description: "",
      IconComponent: FoldIcon,
      hidden: expanded,
      hiddenInContextMenu: true
    },
    {
      key: "CLOSE_NARROWERS",
      label: t("SCHEME.ACTIONS.CLOSE_NARROWERS"),
      description: "",
      IconComponent: UnfoldIcon,
      hidden: !expanded,
      hiddenInContextMenu: true
    },
    {
      key: "VIEW_DETAILS",
      label: t("SCHEME.ACTIONS.VIEW_DETAILS"),
      description: "",
      tracked: true,
      IconComponent: OpenIcon,
      hiddenInContextMenu: true
    },
    {
      key: "SHOW_CONTEXT_MENU",
      label: t("SCHEME.ACTIONS.SHOW_CONTEXT_MENU"),
      description: "",
      tracked: true,
      IconComponent: MenuOpenIcon,
      hiddenInContextMenu: true
    },
    {
      key: SchemeActions.EDIT,
      label: t("SCHEME.ACTIONS.EDIT"),
      description: "",
      IconComponent: EditIcon,
      tracked: true,
      renderActionView: ({scheme}) => <SchemeEdit id={scheme?.id} />
    },
    {
      key: SchemeActions.ADD_CHILDREN,
      label: t("SCHEME.ACTIONS.ADD_CONCEPT"),
      description: "",
      IconComponent: AddIcon,
      tracked: true,
      renderActionView: ({vocabulary, scheme}) => <ConceptCreate vocabulary={vocabulary} scheme={scheme} topInScheme />
    },
    {
      key: SchemeActions.REMOVE,
      label: t("SCHEME.ACTIONS.REMOVE"),
      description: "",
      IconComponent: RemoveIcon,
      renderActionView: ({scheme}) => (
        <ObjectRemovePanel
          warningMessage={t("SCHEME.REMOVE.WARNING_MESSAGE")}
          successMessage={t("SCHEME.REMOVE.SUCCESS_MESSAGE")}
          objectId={scheme.id}
          refetchQueries={["Schemes"]}
        />
      ),
      tracked: true,
      hiddenInFabMenu: true
    }
  ];

  if (extendSchemeActions) {
    actions = extendSchemeActions({actions, t, expanded, scheme});
  }

  return actions.filter((action) => !(action.hidden || false));
}

/**
 *
 */
export default function SchemeSidePopupActions({scheme, vocabulary} = {}) {

  const {t} = useTranslation();
  const actions = generateSchemeActions({t, scheme}).filter(({hiddenInContextMenu}) => !hiddenInContextMenu);
  const [selectedAction, setSelectedAction] = useState();
  const [actionType, setActionType] = useTrackedAction();

  useEffect(() => {
    setSelectedAction(actions.find(({key}) => key === actionType));
  }, [actionType]);

  return (
    <>
      <Grid item xs sx={{padding: 2}}>
        <Typography variant={"h5"} gutterBottom>
          {scheme?.prefLabel || <Skeleton variant="text" />}
        </Typography>

        <Divider />
        <MenuList>
          {actions.map((action) => {
            const {key, label, description, IconComponent} = action;

            return (
              <MenuItem
                key={key}
                onClick={() => handleClickAction({action})}
                selected={selectedAction?.key === key}>
                <ListItemIcon>
                  <IconComponent />
                </ListItemIcon>
                {label}
              </MenuItem>
            );
          })}
        </MenuList>
      </Grid>
      <If condition={selectedAction}>
        <Grid item xs={7} sx={{p: 2}}>
          <If condition={selectedAction}>{selectedAction.renderActionView?.({scheme, vocabulary})}</If>
        </Grid>
      </If>
    </>
  );

  function handleClickAction({action}) {
    setSelectedAction(action);
    setActionType(action.key);

    if (action.tracked) {
      setActionType(action.key);
    }
  }
}
