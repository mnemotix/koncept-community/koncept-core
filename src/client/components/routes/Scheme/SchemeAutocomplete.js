import {Autocomplete} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {ListItemText} from "@mui/material";
import {gqlSchemes} from "./gql/Schemes.gql";


/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} gqlVariables
 * @param {object} selectedObject
 * @param {string} error
 * @param {boolean} creationEnabled
 * @param {function} onCreate
 */

export function SchemeAutocomplete({
  onSelect,
  placeholder,
  selectedObject,
  error,
  creationEnabled,
  onCreate,
  gqlVariables
} = {}) {

  const {t} = useTranslation();

  return (
    <Autocomplete
      placeholder={placeholder || t("SCHEME.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlSchemes}
      gqlVariables={gqlVariables}
      gqlDataConnectionPath={"schemes"}
      optionLabelPath={"prefLabel"}
      onSelect={onSelect}
      selectedObject={selectedObject}
      creationEnabled={creationEnabled}
      onCreate={onCreate}
      AutocompleteProps={{
        noOptionsText: t("SCHEME.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
      renderOption={(scheme) => <ListItemText primary={scheme.prefLabel} />}
    />
  );
}
