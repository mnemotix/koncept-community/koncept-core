import {string} from "prop-types";
import {Typography, List, ListItem, Box, ListItemText, Tooltip, Divider} from "@mui/material";
import Skeleton from '@mui/material/Skeleton';

import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {gqlScheme} from "./gql/Scheme.gql";
import {memo} from "react";
import {pickedPropsAreEquals} from "../../../utilities/pickedPropsAreEquals";

const classes = {
  empty: {
    color: "text.emptyHint"
  },
  propertyItem: {
    marginTop: 3
  },
};

const schemeProperties = [];

/**
 *
 */
const SchemeProperties = memo(({id, prefLabel} = {}) => {

  const {t} = useTranslation();
  const {data: {scheme} = {}, loading} = useQuery(gqlScheme, {
    variables: {id}
  });

  return (
    <>
      <Typography variant={"h5"} gutterBottom>
        {scheme?.prefLabel || prefLabel || <Skeleton variant="text" />}
      </Typography>

      <Divider />

      {schemeProperties.map(({key, i18n, toolTip}) => (
        <Box key={key} sx={classes.propertyItem}>
          <Tooltip title={toolTip} placement="left">
            <Typography variant={"h6"} gutterBottom>
              {t("CONCEPT.PROPERTIES." + i18n)}
            </Typography>
          </Tooltip>

          <Choose>
            <When condition={loading}>
              <Skeleton variant="text" />
            </When>
            <When condition={Array.isArray(scheme?.[key]) && scheme?.[key].length > 0}>
              <List dense disablePadding>
                {scheme[key].map((label) => {
                  return (
                    <ListItem key={label} disableGutters>
                      <ListItemText primary={label} />
                    </ListItem>
                  );
                })}
              </List>
            </When>
            <When condition={scheme?.[key] && !Array.isArray(scheme?.[key])}>{scheme?.[key]}</When>
            <Otherwise>
              <Box component="span" sx={classes.empty}>{t("CONCEPT.EMPTY_PROPERTY")}</Box>
            </Otherwise>
          </Choose>
        </Box>
      ))}
    </>
  );
}, pickedPropsAreEquals(["id"]));

SchemeProperties.propTypes = {
  id: string.isRequired,
  prefLabel: string
};

export default SchemeProperties;
