import {DynamicForm} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useMutation, useQuery} from "@apollo/client";
import {useSnackbar} from "notistack";
import Skeleton from '@mui/material/Skeleton';

import {schemeFormDefinition} from "./form/Scheme.form";
import {gqlUpdateScheme} from "./gql/UpdateScheme.gql";
import {SchemeForm} from "./SchemeForm";
import {gqlScheme} from "./gql/Scheme.gql";

/**
 *
 */
export const SchemeEdit = ({id} = {}) => {

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  const {data: {scheme} = {}, loading} = useQuery(gqlScheme, {
    variables: {id}
  });

  const [mutateScheme, {loading: saving}] = useMutation(gqlUpdateScheme, {
    onCompleted: (data) => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });

  return (
    <DynamicForm
      singleColumnLayout
      object={scheme}
      formDefinition={schemeFormDefinition}
      mutateFunction={mutateScheme}
      saving={saving}
    >
      <Choose>
        <When condition={loading}>
          <Skeleton variant="text" />
        </When>
        <Otherwise>
          <SchemeForm />
        </Otherwise>
      </Choose>
    </DynamicForm>
  );
};
