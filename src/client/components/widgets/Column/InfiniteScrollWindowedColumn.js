import {func, bool, array, number} from "prop-types";
import {useTheme} from '@mui/material';
import {useTranslation} from "react-i18next";
import {VariableSizeList as List} from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import InfiniteLoader from "react-window-infinite-loader";



/**
 * @param {function} renderItem - Callback function responsible for rendering an item
 * @param {boolean} hasNextPage - Are there more items to load?
 * @param {boolean} isNextPageLoading - Are we currently loading a page of items?
 * @param {array} items - Array of items loaded so far.
 * @param {number} itemCount - Total count
 * @param {function} loadNextPage - Callback function responsible for loading the next page of items.
 * @param {function} [getItemHeight] - Returns an iterm height in pixel
 * @return {JSX.Element}
 * @constructor
 */
export function InfiniteScrollWindowedColumn({
  renderItem,
  hasNextPage,
  isNextPageLoading,
  items,
  itemCount,
  loadNextPage,
  getItemHeight
} = {}) {
  const theme = useTheme();

  const {t} = useTranslation();

  // Only load 1 page of items at a time.
  // Pass an empty callback to InfiniteLoader in case it asks us to load more than once.
  const loadMoreItems = isNextPageLoading ? () => { } : loadNextPage;

  // Every row is loaded except for our loading indicator row.
  const isItemLoaded = index => !hasNextPage || index < items.length;

  // Render an item or a loading indicator.
  const Item = ({index, style}) => {
    let content;
    if (!isItemLoaded(index)) {
      content = "Loading...";
    } else {
      content = items[index].name;
    }

    return <div style={style}>{content}</div>;
  };

  return (
    <InfiniteLoader isItemLoaded={isItemLoaded} itemCount={itemCount} loadMoreItems={loadMoreItems}>
      {({onItemsRendered, ref}) => (
        <AutoSizer>
          {({height, width}) => (
            <List
              className="List"
              height={height}
              width={width}
              itemCount={itemCount}
              estimatedItemSize={theme.spacing(5)}
              itemSize={getItemHeight || (() => theme.spacing(5))}
              onItemsRendered={onItemsRendered}
              ref={ref}>
              {renderItem}
            </List>
          )}
        </AutoSizer>
      )}
    </InfiniteLoader>
  );
}

InfiniteScrollWindowedColumn.propTypes = {
  renderItem: func.isRequired,
  hasNextPage: bool.isRequired,
  isNextPageLoading: bool.isRequired,
  items: array.isRequired,
  itemCount: number.isRequired,
  loadNextPage: func.isRequired
};
