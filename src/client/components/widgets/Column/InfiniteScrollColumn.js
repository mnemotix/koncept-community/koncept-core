import {useEffect} from "react";
import {func, bool, number} from "prop-types";
import {List, ListItem, ListItemText} from "@mui/material";
import Skeleton from '@mui/material/Skeleton';
import {useInView} from 'react-intersection-observer';

/**
 * @param {*} children
 * @param {boolean} loading
 * @param {boolean} hasNextPage - Are there more items to load?
 * @param {function} onLoadNextPage
 * @param {boolean} dense
 * @param {number} totalCount
 * @return {JSX.Element}
 * @constructor
 */
export function InfiniteScrollColumn({
  children,
  loading,
  hasNextPage,
  onLoadNextPage,
  dense,
  totalCount
} = {}) {
  const {ref, inView} = useInView({
    /* Optional options */
    threshold: 0,
  });

  useEffect(() => {
    /* Load all pages automatically, no infinite scroll or explicit pagination yet */
    if (!loading && hasNextPage) {
      onLoadNextPage();
    }
  }, [inView]);

  return (
    <List dense={dense}>

      {children}

      <Choose>
        <When condition={loading}>
          {[...Array(Math.min(totalCount ?? 10, 20))].map((_, index) => {
            return (
              <ListItem key={index}>
                <ListItemText primary={<Skeleton />} secondary={<Skeleton width={"50%"} />} />
              </ListItem>
            )
          })}
        </When>
        <Otherwise>
          <If condition={hasNextPage}>
            <div ref={ref} />
          </If>
        </Otherwise>
      </Choose>
    </List>
  );
}

InfiniteScrollColumn.propTypes = {
  loading: bool.isRequired,
  hasNextPage: bool,
  onLoadNextPage: func.isRequired,
  totalCount: number
};
