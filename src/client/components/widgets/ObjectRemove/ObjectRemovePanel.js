import {Button} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useMutation, useApolloClient} from "@apollo/client";
import {useSnackbar} from "notistack";

import {gqlRemoveObject} from "./gql/ObjectRemove.gql";
import {Grid, Typography} from "@mui/material";
import {useTrackedObject} from "../../../hooks/useTrackedObject";

/**
 *
 */
export function ObjectRemovePanel({
  objectId,
  doubleCheck,
  warningMessage,
  successMessage,
  onSuccess,
  refetchQueries
} = {}) {
  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const [, setTrackedObject] = useTrackedObject();
  const apolloClient = useApolloClient();

  const [removeObject, {loading}] = useMutation(gqlRemoveObject);

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Typography variant="subtitle1">{warningMessage}</Typography>
      </Grid>

      <Grid item xs={12}>
        <Button loading={loading} variant="contained" color="secondary" onClick={handleRemove}>
          {t("ACTIONS.REMOVE")}
        </Button>
      </Grid>
    </Grid>
  );

  async function handleRemove() {
    await removeObject({
      variables: {
        input: {
          objectId
        }
      }
    });

    enqueueSnackbar(successMessage || t("ACTIONS.SUCCESS"), {variant: "success"});
    setTrackedObject(null);
    onSuccess?.();


    setTimeout(async () => {
      await apolloClient.refetchQueries({
        include: refetchQueries || [],
        updateCache(cache) {
          cache.evict({ id: objectId });
        },
      });
    }, 2000);
  }
}
