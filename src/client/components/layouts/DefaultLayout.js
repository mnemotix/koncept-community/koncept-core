/*
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {useApolloClient} from "@apollo/client";
import {getUserAuthenticationService} from "@synaptix/ui";
import {
  Box, AppBar, FormControlLabel, CssBaseline, Divider, IconButton, Menu, MenuItem, Toolbar,
  Typography, Link as DefaultLink, Checkbox
} from "@mui/material";
import {AccountCircle as AccountCircleIcon, Mood as MoodIcon} from "@mui/icons-material";
import {Helmet} from "react-helmet";


import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import {ROUTES} from "../../routes";
import {Version} from "../widgets/Version";
import favicon from "../../../../assets/mnx-favicon.png";
import logo from "../../../../assets/mnx-logo.png";
import {useExpertMode} from "../../hooks/useExpertMode";


const appBarHeight = 8;
const footerBarHeight = 4;

const classes = {
  appBar: (theme) => ({
    height: theme.spacing(appBarHeight)
  }),
  toolbar: (theme) => ({
    minHeight: theme.spacing(appBarHeight),
    paddingRight: 4 // keep right padding when drawer closed
  }),
  title: {
    flexGrow: 1
  },
  titleLink: {
    color: "inherit",
    textDecoration: "none",
    display: "flex",
    alignItems: "center"
  },
  titleText: (theme) => ({
    fontVariant: "petite-caps",
    fontSize: theme.typography.pxToRem(26)
  }),
  logo: (theme) => ({
    width: theme.spacing(8),
    height: "auto",
    verticalAlign: "middle",
    padding: theme.spacing(1, 2, 1, 0)
  }),
  content: (theme) => ({
    height: `calc(100vh - ${theme.spacing(appBarHeight)} - ${theme.spacing(footerBarHeight)})`,
    overflow: "auto",
    backgroundColor: "#FAFAFA"

  }),
  footer: (theme) => ({
    height: theme.spacing(footerBarHeight),
    borderTop: "1px solid #eee",
    fontSize: theme.typography.fontSize * 0.9,
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 2),
    boxShadow: theme.shadows[2],
    background: theme.palette.footer?.background || theme.palette.grey[100]
  }),
  mood: (theme) => ({
    margin: theme.spacing(0, 1)
  }),
  version: {
    justifySelf: "end",
    display: "block",
    whiteSpace: "no-wrap"
  },
  developpedBy: (theme) => ({
    flexGrow: 1,
    textAlign: "right",
    borderRight: `2px solid ${theme.palette.grey[400]}`,
    marginRight: theme.spacing(1),
    paddingRight: theme.spacing(1)
  })
};


/**
 * @param {Component} [TitleComponent]
 * @param children
 * @return {*}
 * @constructor
 */
export function DefaultLayout({TitleComponent, children}) {

  const {t} = useTranslation();
  const [expertMode, setExpertMode] = useExpertMode();
  const [anchorEl, setAnchorEl] = useState(null);
  const userAuthenticationService = getUserAuthenticationService({
    apolloClient: useApolloClient()
  });
  let {user, isLogged} = userAuthenticationService.useLoggedUser();

  const {logout} = userAuthenticationService.useLogout();

  return (
    <>
      <CssBaseline />
      <AppBar position="relative" sx={classes.appBar}>
        <Toolbar sx={classes.toolbar} variant="dense">
          <Typography component="h1" variant="h6" color="inherit" noWrap sx={classes.title}>
            <Link to={ROUTES.INDEX} sx={classes.titleLink}>
              {TitleComponent || (
                <>
                  <Helmet titleTemplate={"%s - Koncept"}>
                    <meta charSet="utf-8" />
                    <title>{t("APP_BAR.HOME")}</title>
                    <link rel="icon" type="image/png" href={favicon} sizes="32x32" />
                  </Helmet>
                  <Box component="img" sx={classes.logo} src={logo} alt={"Logo Mnémotix"} />
                  <Box component="span" sx={classes.titleText}>Koncept</Box>
                </>
              )}
            </Link>
          </Typography>

          <FormControlLabel
            control={<Checkbox checked={!!expertMode} onChange={() => setExpertMode(!expertMode)} name="ExpertMode" />}
            label={t("ACTIONS.EXPERT_MODE")}
          />

          <If condition={isLogged}>
            <div>
              <IconButton color="inherit" onClick={(e) => setAnchorEl(e.currentTarget)}>
                <AccountCircleIcon />
              </IconButton>

              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                keepMounted
                open={!!anchorEl}
                onClose={() => setAnchorEl(null)}>
                <MenuItem disabled key="KMenuItem1">
                  {t("APP_BAR.MENU.HELLO")} {user.firstName} <MoodIcon sx={classes.mood} /> !
                </MenuItem>

                <MenuItem disabled key="KMenuItem2">
                  <div style={{width: "100%"}}>
                    <Divider />
                  </div>
                </MenuItem>

                <MenuItem key="KMenuItem3" onClick={() => {logout();}}>
                  {t("APP_BAR.MENU.SIGN_OUT")}
                </MenuItem>
              </Menu>
            </div>
          </If>
        </Toolbar>
      </AppBar>
      <Box component="main" sx={classes.content}>{children}</Box>
      <Box component="footer" sx={classes.footer}>
        <Box sx={classes.developpedBy}>
          Conçu et développé par&nbsp;
          <DefaultLink href={"https://www.mnemotix.com"} target={"_blank"}>
            Mnemotix
          </DefaultLink>
        </Box>

        <Version sx={classes.version} />
      </Box>
    </>
  );
}
