/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import reduce from "lodash/reduce";
import set from "lodash/set";
import get from "lodash/get";
import isEqual from "lodash/isEqual";

/**
 * This utility is used in React.memo second parameter and enable to compare
 * prevProps and nextProps on selected keys.
 *
 * @param {string[]} compareOnKeys
 */
export function pickedPropsAreEquals(compareOnKeys) {
  return (prevProps, nextProps) => {
    return isEqual(deepPick(prevProps, compareOnKeys), deepPick(nextProps, compareOnKeys));
  };
}

/**
 * Enhanced lodash pick function with nested paths like ['a', 'c.d']
 *
 * @param {string[]}  paths
 * @param {object}  object
 * @returns {*}
 */
function deepPick(object, paths) {
  return reduce(paths, (o, p) => set(o, p, get(object, p)), {});
}
