/*
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {Suspense} from "react";
import {SplashScreen, getApolloClient, ErrorBoundary} from "@synaptix/ui";
import {render} from "react-dom";
import {BrowserRouter} from "react-router-dom";
import {ApolloProvider} from "@apollo/client";
import loadable from "@loadable/component";
import {SnackbarProvider, useSnackbar} from "notistack";
import {i18nInitialize} from "../locales/i18nInitialize";
import {generateTheme} from "./generateTheme";
import {ThemeProvider} from '@mui/material/styles';
import {createGenerateClassName} from '@mui/styles';
import {StylesProvider} from '@mui/styles';


const Koncept = loadable(() => import(/* webpackChunkName: "Koncept" */ "./Koncept"));

let reactRootElement = document.getElementById("react-root");

const ApolloContainer = ({i18n, children, possibleTypes}) => {
  const {enqueueSnackbar} = useSnackbar();
  return <ApolloProvider client={getApolloClient({i18n, enqueueSnackbar, possibleTypes})}>{children}</ApolloProvider>;
};

/**
 * @param {string} name
 * @param {object} possibleTypes
 * @param {[Extension]} [extensions]
 * @param {Theme} [theme]
 * @param {Component} [AppBarTitleComponent]
 * @param {string} [locationBaseUrl]
 * @param {Component} [SplashScreenComponent]
 */
export function launchKoncept({
  name,
  possibleTypes,
  extensions = [],
  theme,
  AppBarTitleComponent,
  SplashScreenComponent = SplashScreen,
  locationBaseUrl
} = {}) {
  i18nInitialize().then((i18n) => {
    render(
      <BrowserRouter basename={locationBaseUrl}>
        <StylesProvider
          generateClassName={createGenerateClassName({
            productionPrefix: name || Date.now()
          })}>
          <ThemeProvider theme={generateTheme({extraTheme: theme})}>
            <SnackbarProvider maxSnack={3}>
              <ApolloContainer i18n={i18n} possibleTypes={possibleTypes}>
                <Suspense fallback={<SplashScreenComponent />}>
                  <ErrorBoundary>
                    <Koncept
                      extensions={extensions}
                      AppBarTitleComponent={AppBarTitleComponent}
                      SplashScreenComponent={SplashScreenComponent}
                    />
                  </ErrorBoundary>
                </Suspense>
              </ApolloContainer>
            </SnackbarProvider>
          </ThemeProvider>
        </StylesProvider>
      </BrowserRouter>,
      reactRootElement
    );
  });
}
