/*
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  useLoggedUser, ExtensionsContext, getExtensionTopRoutes, EnvironmentContext,
  FragmentSwitch,
} from "@synaptix/ui";
import {Route} from "react-router-dom";
import loadable from "@loadable/component";
import {useQuery, gql} from "@apollo/client";
import {ROUTES} from "./routes";
import EnvVars from "../../app/config/environment";

import {DefaultLayout} from "./components/layouts/DefaultLayout";
import {PinnedConceptsProvider} from "./hooks/usePinnedConcepts";
import {Progress} from "./components/widgets/Progress";
import {TrackedActionProvider} from "./hooks/useTrackedAction";
import {TrackedObjectProvider} from "./hooks/useTrackedObject";
import {ExpertModeProvider} from "./hooks/useExpertMode";


const Home = loadable(() => import("./components/routes/Home/Home"));
const Vocabulary = loadable(() => import("./components/routes/Vocabulary/Vocabulary"));

const gqlEnvironmentQuery = gql`
  query EnvironmentQuery {
    environment {
      ${Object.entries(EnvVars)
    .filter(([variable, {exposeInGraphQL}]) => exposeInGraphQL === true)
    .map(([variable]) => variable)}
    }
  }
`;

/**
 * @param extensions
 * @param {[MentionDefinition]} [extraMentionsDefinitions]
 * @param {Component} [AppBarTitleComponent]
 * @param {Component} [SplashScreenComponent]
 * @return {*}
 * @constructor
 */
export default function Koncept({extensions, AppBarTitleComponent, SplashScreenComponent = Progress} = {}) {
  const {data: envData, loading: envLoading} = useQuery(gqlEnvironmentQuery);
  const {isLogged, isAdmin, isContributor, isEditor, loading: userLoading} = useLoggedUser();
  const ExtensionsRoutes = getExtensionTopRoutes({
    extensions,
    props: {
      isAdmin,
      isContributor,
      isEditor
    }
  });

  return userLoading ? (
    <SplashScreenComponent />
  ) : (
    <PinnedConceptsProvider>
      <EnvironmentContext.Provider value={envData?.environment}>
        <ExtensionsContext.Provider value={extensions}>
          <ExpertModeProvider>
            <DefaultLayout TitleComponent={AppBarTitleComponent}>
              <FragmentSwitch>
                {/* Put extensions routes in first place to override following ones in case...*/}
                {ExtensionsRoutes}

                <Route path={ROUTES.VOCABULARY}>
                  <Route
                    render={() => (
                      <TrackedActionProvider>
                        <TrackedObjectProvider>
                          <Vocabulary />
                        </TrackedObjectProvider>
                      </TrackedActionProvider>
                    )}
                  />
                </Route>

                <Route component={Home} />
              </FragmentSwitch>
            </DefaultLayout>
          </ExpertModeProvider>
        </ExtensionsContext.Provider>
      </EnvironmentContext.Provider>
    </PinnedConceptsProvider>
  );
}
